using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnPutDown : MonoBehaviour
{
    public AudioSource sound;

    void OnMouseUp()
    {
        if(!sound.isPlaying)
        {
            sound.pitch = UnityEngine.Random.Range(0.95f, 1.11f);
        }
        sound.Play();
    }
}
