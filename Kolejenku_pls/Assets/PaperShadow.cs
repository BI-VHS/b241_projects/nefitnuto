using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaperShadow : MonoBehaviour
{
    public GameObject shadow;
    PaperSpriteChanger paperSpriteChanger;

    void Start()
    {
        paperSpriteChanger = gameObject.GetComponent<PaperSpriteChanger>();
    }

    void OnMouseDown()
    {
        if(paperSpriteChanger.spriteType == PaperSpriteChanger.SpriteType.FULL)
        {
            shadow.SetActive(true);
        }
    }

    void OnMouseUp()
    {
        if(paperSpriteChanger.spriteType == PaperSpriteChanger.SpriteType.FULL)
        {
            shadow.SetActive(false);
        }
    }
}
