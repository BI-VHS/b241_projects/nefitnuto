using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class ChangeDate : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<TextMeshProUGUI>().text = Convert.ToString(20 + GameObject.Find("GameState").GetComponent<GameState>()[STATE.DAY]);   
    }
}
