using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameMusicController : MonoBehaviour
{
    public AudioSource normal_music;
    public AudioSource unique_music;

    [Space]

    public PeopleAtDesks people;

    void Start()
    {
        normal_music.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if(people.GetCurrentPerson() != null)
        if(people.GetCurrentPerson().GetPerson().isUnique)
        {
            if(normal_music.isPlaying)
            {
                normal_music.Pause();
            }
            if(!unique_music.isPlaying)
            {
                unique_music.Play();
            }
        }
        else
        {
            if(unique_music.isPlaying)
            {
                unique_music.Stop();
            }
            if(!normal_music.isPlaying)
            {
                normal_music.UnPause();            
            }
        }
    }
}
