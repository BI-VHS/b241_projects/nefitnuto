using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueDataset1
{
    public List<Entry> entry;
    public List<Entry> deny;
    public List<Entry> accept;
    public Questioning questioning;

    [Serializable]
    public class Entry
    {
        public string character;
        public List<List<string>> lines;
    }

    [Serializable]
    public class Questioning
    {
        public DocumentType missing_document;
        public WrongInformation wrong_information;
    }

    [Serializable]
    public class DocumentType
    {
        public List<Entry> kolejenka;
        public List<Entry> id_card;
        public List<Entry> passport;
        public List<Entry> confirmation_of_study;
    }

    [Serializable]
    public class WrongInformation
    {
        // Add fields as needed
    }
}
