using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableMusicAtEnable : MonoBehaviour
{
    void OnEnable()
    {
        GameObject.Find("Music").SetActive(false);
    }
}
