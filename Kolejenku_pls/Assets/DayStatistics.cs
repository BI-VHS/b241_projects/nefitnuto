using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DayStatistics : MonoBehaviour
{
    public int person_count;
    public int money_made;
    public int fines_count;
    public int fines_price;
}
