using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableSoundsAtTimer : MonoBehaviour
{
    public Timer timer;
    public AudioSource[] sounds;

    void Update()
    {
        if(timer.secondsLeft == 0)
        {
            foreach(var x in sounds)
            {
                x.enabled = false;
            }
        }
    }
}
