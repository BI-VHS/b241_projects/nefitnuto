using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectPersistance : MonoBehaviour
{
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
