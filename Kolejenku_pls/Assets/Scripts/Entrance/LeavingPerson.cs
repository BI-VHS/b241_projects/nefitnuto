using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;

public class LeavingPerson : MonoBehaviour {
    /// <summary>
    /// Animation sequences
    /// </summary>
    private Animator animator;
    /// <summary>
    /// Currently selected child waypoint, where is person heading to
    /// </summary>
    private GameObject currentWaypoint;
    /// <summary>
    /// index of current waypoint
    /// </summary>
    private int childIndex;
    /// <summary>
    /// Parent Object which holds all waypoints as its children
    /// </summary>
    public List<GameObject> waypoints = new();
    public float movementSpeed;

    private void Start() {
        animator = GetComponent<Animator>();
        // Flip person by X axis
        Vector3 currentScale = transform.localScale;
        currentScale.x *= -1;
        transform.localScale = currentScale;
        animator.SetTrigger("Move");
        childIndex = 0;
    }

    /// <summary>
    /// Removes person from the scene, when object is no longer needed
    /// </summary>
    public void DespawnPerson() {
        Destroy(gameObject);
    }

    /// <summary>
    /// Gets next waypoint, when person has already reached previous waypoint
    /// </summary>
    private void GetNextWaypoint() {
        if(childIndex < waypoints.Count) {
            currentWaypoint = waypoints[childIndex++];
            return;
        }
        DespawnPerson();
    }

    // Update is called once per frame
    void Update() {
        if(currentWaypoint == null || transform.position == currentWaypoint.transform.position) {
            GetNextWaypoint();
        }
        transform.position = Vector3.MoveTowards(transform.position, currentWaypoint.transform.position, movementSpeed * Time.deltaTime);
    }

    public void SetWaypoints(int index, GameObject currentWaypoint) {
        this.childIndex = index;
        this.currentWaypoint = currentWaypoint;
    }
}
 