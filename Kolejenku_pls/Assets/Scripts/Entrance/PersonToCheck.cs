using System;
using System.Collections.Generic;
using UnityEngine;

public class PersonToCheck : MonoBehaviour
{
    public PaperLegality paperLegality = new PaperLegality();
    public bool illegal = false;
    public bool is_visitor = false;
    public bool hasFake = false;
    public bool isUnique = false;
    public string firstName;
    public string lastName;
    public string fake_firstName;
    public string fake_lastName;

    //True = male, False = female
    public bool gender;
    public string nationality;
    public string country;
    public bool registered;
    public int blockNumber;
    public int roomNumber;
    public string school;
    public string id;
    [SerializeField]
    public List<Paper> documents;
    private List<GameObject> gameObject_documents;
    private Paper kolejenka;
    private bool is_kolejenka_printed = false;
    // key = when to trigger dialogue (example: 'first meet', 'no documents', ...), value = dialogue itself
    static public DialogueDataset dialogue { get; set; }
    public UniqueDialogueDataset unique_dialogue { get; set; }
    public AudioSource unique_voice;

    [SerializeField]
    public TextAsset unique_dialogue_json;

    void Start()
    {
        dialogue ??= JsonUtility.FromJson<DialogueDataset>(Resources.Load<TextAsset>("generic").text);

        if(unique_dialogue_json)
        {
            unique_voice = gameObject.GetComponent<AudioSource>();

            unique_dialogue = JsonUtility.FromJson<UniqueDialogueDataset>(unique_dialogue_json.text);
        }
    
        gameObject_documents = new List<GameObject>();
    }

    public bool HasDocument(Paper document) { 
        return documents.Contains(document);
    }

    public void SpawnDocuments()
    {
        if(isUnique)
        {
            gameObject.GetComponent<UniquePaperNameFormatter>().ReformatPaper();
        }
        // ? maybe already create the papers, just make them inactive?
        foreach(var doc in documents)
        {
            if(!registered && doc is Kolejenka)
            {
                kolejenka = (Kolejenka)doc;
                continue;
            }
            gameObject_documents.Add(doc.Create());
        }
    }

    public bool IsKolejenkaPrinted()
    {
        if(!kolejenka)
        {
            return true;
        }

        return is_kolejenka_printed;
    }

    public void PrintKolejenka()
    {
        is_kolejenka_printed = true;
        var ko = kolejenka.Create();
        ko.transform.parent = GameObject.Find("Kolejenka Spawn Point").transform;
        ko.GetComponent<PaperSpriteChanger>().switchToFullSprite();
        ko.GetComponent<Animator>().enabled = true;
        ko.GetComponent<Animator>().SetTrigger("play");
        gameObject_documents.Add(ko);
    }

    public List<GameObject> GetGameObjectDocuments()
    {
        return gameObject_documents;
    }

    public void DestroyDocuments()
    {
        foreach(var paper in gameObject_documents)
        {
            Destroy(paper);
        }
        gameObject_documents.Clear();
    }

    public void print()
    {
        print(
            "Name: " + firstName + " " + lastName + "\n" +
            "ID: " + Convert.ToString(id) + "\n" +
            "Nationality: " + nationality + "\n"
        );
    }
}
