using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using Unity.VisualScripting;
using UnityEngine;

/// <summary>
/// Supportive class which stores all the important data about the person, who is waiting to be let inside
/// </summary>
public class PersonStatistics {
    /// <summary>
    /// Position where should object deactive itself when person was not allowed to enter
    /// </summary>
    private Vector3 endingPositionWhenRejected;
    /// <summary>
    /// Position where is person waiting to be checked
    /// </summary>
    private Vector3 checkPosition;
    /// <summary>
    /// Position in scene where should object deactive itself when person was alloved to enter
    /// </summary>
    private Vector3 endingPositionWhenAccepted;
    /// <summary>
    /// Specific person instance
    /// </summary>
    private PersonToCheck person;
    /// <summary>
    /// Person's walking speed
    /// </summary>
    private float movementSpeed;
    /// <summary>
    /// Remaining cooldown before the next animation should take place
    /// </summary>
    private float currentCooldown;
    /// <summary>
    /// Bool variables which only serves as support variables when deciding direction of object's movement
    /// </summary>
    private bool isMovingRight = true;
    private bool isMovingUp = false;

    public PersonStatistics(Vector3 endingPositionWhenRejected,
                            Vector3 checkPosition,
                            Vector3 endingPositionWhenAccepted, 
                            PersonToCheck person,
                            float movementSpeed) {
        this.endingPositionWhenRejected = endingPositionWhenRejected;
        this.checkPosition = checkPosition; 
        this.endingPositionWhenAccepted = endingPositionWhenAccepted;
        this.person = person;
        this.movementSpeed = movementSpeed;
        this.currentCooldown = 0;
    }

    /// <summary>
    /// Changes direction where person is looking at
    /// </summary>
    public void FlipPerson() {
        if (person == null) return;
        Vector3 currentScale = person.transform.localScale;
        currentScale.x *= -1;
        person.transform.localScale = currentScale;
        isMovingRight = false;
    }

    /// <summary>
    /// Handles walking and breathing animation
    /// </summary>
    /// <param name="animationSpeed"> How far person wants to move from current location </param>
    /// <param name="deltaMovement"> Maximum y coordinate for walking and breathing animation </param>
    /// <param name="cooldown"> Cooldown for breathing animation to be repeated </param>
    public void MoveUpAndDown(float animationSpeed, float deltaMovement, float cooldown) {
        // Action not yet to be repeated
        if (currentCooldown < cooldown) {
            currentCooldown += Time.deltaTime;
            return;
        }
        bool upwardsMovement = person.transform.position.y + animationSpeed * Time.deltaTime < deltaMovement + endingPositionWhenRejected.y;
        bool downwardsMovement = person.transform.position.y - animationSpeed * Time.deltaTime > endingPositionWhenRejected.y;
        if (upwardsMovement && isMovingUp) {
            Vector3 destinaton = new(person.transform.position.x, deltaMovement + endingPositionWhenRejected.y, person.transform.position.z);
            person.transform.position = Vector3.MoveTowards(person.transform.position, destinaton, animationSpeed * Time.deltaTime);
            return;
        }
        // Person has already reached given maximum
        if (!upwardsMovement && isMovingUp) {
            currentCooldown = 0;
            isMovingUp = false;
            return;
        }
        // Person has already reached given minimum
        if (!downwardsMovement && !isMovingUp) {
            currentCooldown = 0;
            isMovingUp = true;
            return;
        }
        Vector3 destination = new(person.transform.position.x, endingPositionWhenRejected.y, person.transform.position.z);
        person.transform.position = Vector3.MoveTowards(person.transform.position, destination, animationSpeed * Time.deltaTime);
        
    }

    /// <summary>
    /// Getters
    /// </summary>
    /// <returns> All the private variables values </returns>
    public Vector3 GetEndingPositionWhenRejected() { return endingPositionWhenRejected; }
    public Vector3 GetCheckPosition() {  return checkPosition; }
    public Vector3 GetEndingPositionWhenAccepted() { return endingPositionWhenAccepted; }
    public PersonToCheck GetPerson() { return person;}
    public float GetMovementSpeed() { return movementSpeed; }
    public bool IsMovingRight() {  return isMovingRight; }
    
}

/// <summary>
/// Handles movement of all persons in left down corner in  the entrance scene
/// </summary>
public class PeopleAtDesks : MonoBehaviour {
    [SerializeField] public bool isNotAllowedToEnter = false;
    [SerializeField] public bool isAllowedToEnter = false;
    /// <summary>
    /// Refence to Queue Handler object
    /// </summary>
    [SerializeField] private GameObject peopleQueue;
    /// <summary>
    ///  List of waypoints for person, who is currently being checked
    /// </summary>
    [SerializeField] public List<GameObject> personToCheckWaypoints;
    /// <summary>
    /// List of waypoints for leaving people who were not allowed to enter
    /// </summary>
    [SerializeField] private List<GameObject> leavingPersonWaypoints;
    /// <summary>
    /// Persons, who will be attempting to get inside
    /// </summary>
    [SerializeField] private List<PersonToCheck> personsToAdd;
    /// <summary>
    /// Parent object for leaving people who were not allowed to enter
    /// </summary>
    [SerializeField] private GameObject parentObject;
    /// <summary>
    /// Defines interval between next breath
    /// </summary>
    [SerializeField] private float breathingCooldown;
    /// <summary>
    /// How fast will person inhale/exhale
    /// </summary>
    [SerializeField] private float breathingSpeed;
    /// <summary>
    /// Maximum distance object should move from starting y coordinate while breathing
    /// </summary>
    [SerializeField] private float deltaBreathingOnYAxis;
    /// <summary>
    /// How fast will person move up and down while walking
    /// </summary>
    [SerializeField] private float walkingSpeedOnYAxis;
    /// <summary>
    /// Maximum distance object should move from starting y coordinate while moving 
    /// </summary>
    [SerializeField] private float deltaWalkingOnYAxis;
    /// <summary>
    /// Person's walking speed on x axis
    /// </summary>
    [SerializeField] private float walkingSpeedOnXAxis;
    /// <summary>
    /// Loads a prefab of a leaving person to send a refence to newly created object
    /// representing leaving person
    /// </summary>
    [SerializeField] private GameObject leavingPersonPrefab;
    /// <summary>
    /// Defines all the important waypoints
    /// </summary>
    [SerializeField] private GameObject waypointsWhileLeaving;
    /// <summary>
    /// Counts amount of people who were not allowed to enter
    /// </summary>
    private int objCounter;
    public Queue<PersonToCheck> personsAtQueue;
    /// <summary>
    /// Current person who was just taken out of the waiting queue
    /// </summary>
    private PersonStatistics currentPerson;

    // Start is called before the first frame update
    public void Start() { 
        personsAtQueue = new Queue<PersonToCheck>();
        objCounter = 0;
    }

    /// <summary>
    /// Creating a new instance of person and removing him/her from the queue
    /// </summary>
    private void PreparePerson() {
        PersonToCheck personToCheck = personsAtQueue.Dequeue();
        Vector3 endingPositionWhenNotAccepted = personToCheckWaypoints[0].transform.position;
        Vector3 stopPosition = personToCheckWaypoints[1].transform.position;
        Vector3 endingPositionWhenAccepted = personToCheckWaypoints[2].transform.position;
        currentPerson = new PersonStatistics(endingPositionWhenNotAccepted, stopPosition, endingPositionWhenAccepted, personToCheck, walkingSpeedOnXAxis);
        currentPerson.GetPerson().gameObject.SetActive(true);
    }

    /// <summary>
    /// Removing persons, who have already left the scene
    /// </summary>
    private void RemovePersonFromScene() {
        currentPerson.GetPerson().gameObject.SetActive(false);
        if(isNotAllowedToEnter) {
            //Create new leaving person instance
            GameObject newLeavingPerson = Instantiate(leavingPersonPrefab);
            
            // cursed
            //      pan budouci bakalar si tohle urcite vyresi
            //      protoze tohle je velice neoptimalni reseni
            //      a jelikoz jsi udelal AG1 tak bys tohle mel byt schopnej vyresit :trollvoda:
            SpriteRenderer sr = newLeavingPerson.GetComponent<SpriteRenderer>();
            sr.sortingOrder = 97;
            // end of cursed

            newLeavingPerson.name = "Leaving Person" + objCounter++;
            newLeavingPerson.transform.SetParent(parentObject.transform);
            LeavingPerson currentObj = newLeavingPerson.AddComponent<LeavingPerson>();
            newLeavingPerson.transform.position = leavingPersonWaypoints[0].transform.position;
            currentObj.movementSpeed = 0.60f;
            currentObj.waypoints = leavingPersonWaypoints;
        }
        currentPerson = null;
        isAllowedToEnter = false;
        isNotAllowedToEnter = false;
    }

    // Update is called once per frame
    public void Update() {
        QueueHandler handler = peopleQueue.GetComponent<QueueHandler>();
        if (currentPerson == null) {
            // No new persons to check
            if (personsAtQueue.Count == 0) {
                // if(GameObject.Find("Timer").GetComponent<Timer>().secondsLeft > 0)
                // {
                //     return;
                // }
                // Stop eveyone in queue
                if (handler.SpawnPerson()) {
                    handler.StopMovingEveryoneInQueue();
                    handler.SetSpawnPerson(false);
                }
                return;
            }
            if (handler.SpawnPerson()) {
                handler.StopMovingEveryoneInQueue();
                PreparePerson();
                handler.SetSpawnPerson(false);
            }
            return;
        }
        GameObject personPosition = currentPerson.GetPerson().gameObject;
        float deltaMovement = currentPerson.GetMovementSpeed();
        //Person yet to be checked
        if (!isAllowedToEnter && !isNotAllowedToEnter) {
            if (personPosition.transform.position.x < currentPerson.GetCheckPosition().x) {
                currentPerson.MoveUpAndDown(walkingSpeedOnYAxis, deltaWalkingOnYAxis, 0);
                personPosition.transform.position = Vector3.MoveTowards(personPosition.transform.position, currentPerson.GetCheckPosition(), deltaMovement * Time.deltaTime);
                return;
            }
            currentPerson.MoveUpAndDown(breathingSpeed, deltaBreathingOnYAxis, breathingCooldown);

        } else if(isAllowedToEnter) {
            currentPerson.MoveUpAndDown(walkingSpeedOnYAxis, deltaWalkingOnYAxis, 0);
            if (personPosition.transform.position.x < currentPerson.GetEndingPositionWhenAccepted().x) {
                personPosition.transform.position = Vector3.MoveTowards(personPosition.transform.position, currentPerson.GetEndingPositionWhenAccepted(), deltaMovement * Time.deltaTime);
                return;
            }
            RemovePersonFromScene();
            handler.StartMovingEveryoneInQueue();
        } else if(isNotAllowedToEnter) {
            currentPerson.MoveUpAndDown(walkingSpeedOnYAxis, deltaWalkingOnYAxis, 0);
            if (currentPerson.IsMovingRight()) {
                currentPerson.FlipPerson();
            }
            if (personPosition.transform.position.x > currentPerson.GetEndingPositionWhenRejected().x) {
                personPosition.transform.position = Vector3.MoveTowards(personPosition.transform.position, currentPerson.GetEndingPositionWhenRejected(), deltaMovement * Time.deltaTime);
                return;
            }
            RemovePersonFromScene();
            handler.StartMovingEveryoneInQueue();
        } 
    }

    public void AllowEnter() {
        if (!isNotAllowedToEnter) {
            isAllowedToEnter = true;
            GetComponent<AudioSource>().Play();
        }
    }

    public void DenyEnter() {
        if(!isAllowedToEnter) {
            isNotAllowedToEnter = true;
        }  
    }

    public void MakeDecision(bool allow) {
        if(allow) {
            AllowEnter();
        } else {
            DenyEnter();
        }
    }

    public PersonStatistics GetCurrentPerson() { return currentPerson; }
}
