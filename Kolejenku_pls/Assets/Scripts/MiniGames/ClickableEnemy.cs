using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickableEnemy : MonoBehaviour, IPointerClickHandler {
    [Header("Visual & Audio stuff")]
    public SpriteRenderer spriteRenderer;
    public AudioSource audioSource;
    public Animator animator;
    public ParticleSystem splash;

    [Header("Colliders")]
    public BoxCollider2D box;
    
    private bool isClicked = false;
    public bool pointerIsClose = false;
    public bool isEating = false;

    private void OnMouseEnter() => pointerIsClose = true;

    private void OnMouseExit() => pointerIsClose = false;

    // called when trigger is clicked on
    public void OnPointerClick(PointerEventData eventData) {
        if (!isClicked && eventData.pointerCurrentRaycast.gameObject == gameObject) {
            isClicked = true;
            StartCoroutine(killEnemy());
        }
    }

    public void makeEat() {
        isEating = true;
        StartCoroutine(enemyEating());
    }

    // play death sound, destroy gameobject after sound is over
    IEnumerator killEnemy() {
        splash.Play();
        spriteRenderer.color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        box.enabled = false;

        float delay = 0.5f;
        if (audioSource.clip != null) {
            float pitch = Random.Range(0.9f, 1.1f);
            audioSource.pitch = pitch;
            audioSource.Play();
            delay = audioSource.clip.length + 0.5f;
        }

        yield return new WaitForSeconds(delay);
        Destroy(gameObject);
    }

    IEnumerator enemyEating() {
        animator.SetTrigger("isEating");
        yield return new WaitForSeconds(3f);
        isEating = false;
    }
}
