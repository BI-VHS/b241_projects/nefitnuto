using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Cockroaches : MonoBehaviour {
    [Header("MiniGame settings")]
    public GameObject enemy; // ClickableEnemy: svaby / stenice
    [SerializeField] private int enemyCount = 10;
    [SerializeField] private int enemiesKilled = 0;
    [SerializeField] private int moneyEarned = 0;
    [SerializeField] private int baseEarningPerKill = 100;
    [SerializeField] private GameState gameState;
    [SerializeField] private List<GameObject> enemies = new();
    [SerializeField] private int chanceOfEating = 10; // 1 : chance of eating
    [SerializeField] private float moveSpeed = 2.0f;
    [SerializeField] private float speedMultiplier = 2.0f;
    [SerializeField] private GameObject spawnPoints;
    [SerializeField] private GameObject targetPoints;

    private Dictionary<GameObject, ClickableEnemy> enemyScript = new();

    // each enemy has its own current target point index to track progress
    private Dictionary<GameObject, int> enemyTargetIndex = new();
    private Dictionary<GameObject, Vector3> enemyTargetPos = new();

    // each enemy has its own speed
    private Dictionary<GameObject, float> enemySpeed = new();

    bool isDone = false;

    [Header("Timer")]
    public Timer timer;
    [SerializeField] private float secondsLeft = 10f;

    [Header("MiniGame Summary screen")]
    public GameObject endGameScreen;
    public TextMeshProUGUI[] textObjects; // 1st half left side, 2nd half right side
    [SerializeField] private string[] texts;

    void Start() {
        timer.setTimer(secondsLeft);

        endGameScreen.SetActive(false);
        setTexts();

        // create enemy instances at random spawn points
        for (int i = 0; i < enemyCount; i++) {
            int randomIndex = Random.Range(0, spawnPoints.transform.childCount);
            
            // offset spawn position
            float posOffset = Random.Range(-1f, 1f);
            Vector3 pos = spawnPoints.transform.GetChild(randomIndex).position;
            pos.x += posOffset;

            // instantiate enemies
            GameObject spawnedEnemy = Instantiate(enemy, pos, Quaternion.identity, gameObject.transform);
            enemies.Add(spawnedEnemy);

            // add to enemy script dict
            enemyScript[spawnedEnemy] = spawnedEnemy.GetComponent<ClickableEnemy>();

            // assign random target
            int randomTargetIndex = Random.Range(0, targetPoints.transform.childCount);
            enemyTargetIndex[spawnedEnemy] = randomTargetIndex;

            // offset target position
            Vector3 targetPosition = targetPoints.transform.GetChild(randomTargetIndex).position;
            targetPosition.x += posOffset;
            enemyTargetPos[spawnedEnemy] = targetPosition;
        }

        // assign starting speed
        foreach (GameObject enemy in enemies) {
            float speed = Random.Range(moveSpeed / 2f, moveSpeed * 1.2f);
            enemySpeed[enemy] = speed;
        }

        // assign game state
        GameObject obj = GameObject.FindGameObjectWithTag("GameState");
        if (obj != null)
            gameState = obj.GetComponent<GameState>();
        else gameState = null;
    }

    void Update() {
        if (isDone) return;

        // minigame finished
        if (!timer.timerOn) {
            calculateEarnings();
            showSummary();
            isDone = true;
            return;
        }

        // remove killed enemies
        for (int i = 0; i < enemies.Count; i++) {
            if (enemies[i] == null) {
                enemies.Remove(enemies[i]);
                ++enemiesKilled;
            }
        }

        // end timer if all enemies are dead
        if (enemies.Count <= 0) {
            timer.timerOn = false;
        }

        moveEnemies();
    }

    void moveEnemies() {
        foreach (GameObject enemy in enemies) {
            // let enemy eat lmaooo
            if (enemyScript[enemy].isEating)
                continue;

            int currentTargetIndex = enemyTargetIndex[enemy];
            Vector3 targetPosition = enemyTargetPos[enemy];

            // calculate direction from enemy to target
            Vector3 direction = (targetPosition - enemy.transform.position).normalized;

            // rotate enemy to face target direction
            if (direction != Vector3.zero) {
                float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
                enemy.transform.rotation = Quaternion.Euler(0, 0, angle - 90f);
            }

            // move enemy towards target position
            float speed = enemySpeed[enemy];
            if (enemy.GetComponent<ClickableEnemy>().pointerIsClose)
                speed = moveSpeed * speedMultiplier;

            enemy.transform.position = Vector3.MoveTowards(enemy.transform.position, targetPosition, speed * Time.deltaTime);

            // check if enemy has reached current target point
            if (Vector3.Distance(enemy.transform.position, targetPosition) < 0.1f) {
                int eating = Random.Range(0, chanceOfEating);
                if (eating % chanceOfEating == 0) {
                    enemyScript[enemy].makeEat();
                    continue;
                }

                int randomTargetIndex = Random.Range(0, targetPoints.transform.childCount);

                // move to next target point (ensure it is not the same point)
                if (enemyTargetIndex[enemy] == randomTargetIndex)
                    enemyTargetIndex[enemy] = (randomTargetIndex + 1) % targetPoints.transform.childCount;
                else
                    enemyTargetIndex[enemy] = randomTargetIndex;

                // offset target position
                targetPosition = targetPoints.transform.GetChild(randomTargetIndex).position;
                float posOffset = Random.Range(-1f, 1f);
                targetPosition.x += posOffset;

                enemyTargetPos[enemy] = targetPosition;

                float speedChange = Random.Range(moveSpeed / 2f, moveSpeed * 1.2f);
                enemySpeed[enemy] = speedChange;
            }
        }
    }

    void setTexts() {
        // set all left side text objects to inputed text
        for (int i = 0; i < textObjects.Length / 2; i++)
            textObjects[i].text = texts[i];
    }

    void calculateEarnings() {
        float timeMultiplier = 1.0f;
        float timePassed = timer.getTimePassed();

        // calculate multiplier
        if (timePassed > 0)
            timeMultiplier = Mathf.Max(0.5f, 1.5f - (timePassed / 60f));

        // calculate money earned
        if (timePassed < secondsLeft * 0.5f)
            moneyEarned = Mathf.RoundToInt(enemiesKilled * baseEarningPerKill * timeMultiplier);
        else
            moneyEarned = enemiesKilled * baseEarningPerKill;

        // lose money if no kills
        if (enemiesKilled == 0)
            moneyEarned = -Mathf.RoundToInt(enemyCount * baseEarningPerKill * timeMultiplier);

        // update bank balance
        if (gameState != null)
            gameState[STATE.MONEY] += moneyEarned;
    }

    void showSummary() {
        float time = timer.getTimePassed();

        float minutes = Mathf.FloorToInt(time / 60);
        float seconds = Mathf.FloorToInt(time % 60);
        float milliseconds = Mathf.FloorToInt(time % 1 * 1000f);

        textObjects[3].text = string.Format("{0:00}:{1:00}.{2:000}", minutes, seconds, milliseconds);
        textObjects[4].text = enemiesKilled.ToString() + "/" + enemyCount.ToString();

        if (moneyEarned > 0)
            textObjects[5].text = "+" + moneyEarned.ToString("N0") + " k�";
        else
            textObjects[5].text = moneyEarned.ToString("N0") + " k�";

        endGameScreen.SetActive(true);
        endGameScreen.GetComponent<Animator>().SetTrigger("EndMiniGame");
    }
}
