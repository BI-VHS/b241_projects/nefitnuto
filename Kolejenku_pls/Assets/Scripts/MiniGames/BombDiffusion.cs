using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BombDiffusion : MonoBehaviour {
    [SerializeField] private List<GameObject> screws;
    [SerializeField] private GameObject bombPlate;
    [SerializeField] private GameObject bombPlateEnemy;
    [SerializeField] private List<GameObject> wires;
    [SerializeField] private GameObject bombAllWires;
    [SerializeField] private GameObject gameOver;
    [SerializeField] private GameObject timerObj;
    [SerializeField] private Timer timer;
    [SerializeField] private float timeLeft = 30f;

    private bool revealWires = false;
    private int currentWireIndex = 0;
    private bool isOver = false;

    void Start() {
        // turn off bomb insides
        bombAllWires.SetActive(false);
        foreach (var w in wires)
            w.SetActive(false);
        bombPlateEnemy.SetActive(false);
        gameOver.SetActive(false);

        // turn on timer
        timerObj.SetActive(true);
        timer.setTimer(timeLeft);
        timer.timerOn = true;
    }

    void Update() {
        if (isOver) return;

        if (timer.secondsLeft <= 0f) {
            gameOver.SetActive(true);
            return;
        }

        // remove unscrewed screws
        for (int i = 0; i < screws.Count; i++)
            if (screws[i] == null) {
                screws.Remove(screws[i]);
                break;
            }

        // activate plate if screws are unscrewed
        if (bombPlateEnemy != null && screws.Count == 0) {
            bombPlateEnemy.SetActive(true);
        }

        // activate wires if plate is removed
        if (bombPlateEnemy == null && !revealWires) {
            bombPlate.SetActive(false);
            bombAllWires.SetActive(true);

            revealWires = true;
            foreach (var w in wires)
                w.SetActive(true);
        }

        // check if wires are cut in order
        if (revealWires)
            checkWireCutOrder();
    }

    private void checkWireCutOrder() {
        // ensure there are wires left to check
        if (currentWireIndex < wires.Count) {
            // check if current wire is cut
            if (wires[currentWireIndex] == null) {
                currentWireIndex++; // move to the next wire

                // check if all wires are cut
                if (currentWireIndex == wires.Count) {
                    Debug.Log("Bomb defused!");
                    timer.timerOn = false; // turn off timer
                    isOver = true;
                    timerObj.SetActive(false);
                }
            }
            else {
                // check if wire cut out of order
                for (int i = currentWireIndex + 1; i < wires.Count; i++) {
                    if (wires[i] == null) {
                        gameOver.SetActive(true);
                        isOver = true;
                        timerObj.SetActive(false);
                        break;
                    }
                }
            }
        }
    }

    public void goToMenu() {
        SceneSwitcher switcher = FindAnyObjectByType<SceneSwitcher>();
        switcher.LoadMainMenu();
    }

    public bool isDiffused() => currentWireIndex == wires.Count;
}
