using UnityEngine;
using UnityEngine.EventSystems;

public class ContrabandClick : MonoBehaviour, IPointerClickHandler {
    public bool isClicked = false;

    [Header("Collider")]
    public BoxCollider2D box;

    public void OnPointerClick(PointerEventData eventData) {
        if (!isClicked && eventData.pointerCurrentRaycast.gameObject == gameObject) {
            isClicked = true;
        }
    }
}
