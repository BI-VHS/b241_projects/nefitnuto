using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContrabandChoice : MonoBehaviour {
    [Header ("Settings")]
    [SerializeField] private Image[] sprites;
    [SerializeField] private ContrabandClick[] objects;
    [SerializeField] private GameObject[] indicators;
    public bool hasChosen = false;
    public bool isCorrect = false;
    public bool hasStolenMoney = false;
    public int correctID = -1;
    [SerializeField] private int chosenID = -1;
    [SerializeField] private int moneyID = -1;

    /*
        Sprite Index Explanation:
        if index is -1   == nothing has been assigned => something is wrong lmao (doesnt apply for money)
        if index is 0..2 == one is contraband  => correct answer is given correctID
        if index is -69  == no contraband here => correct answer is close screen
    */

    void Start() {
        // turn off indicators
        foreach (var item in indicators)
            item.SetActive(false);
    }

    void Update() {
        for (int i = 0; i < objects.Length && chosenID == -1; ++i) {
            if (objects[i].isClicked) {
                chosenID = i;
                break;
            }
        }

        // player made a choice
        if (chosenID != -1 && !hasChosen) {
            // correct choice
            if (chosenID == correctID) {
                isCorrect = true;
                indicators[chosenID + 3].SetActive(true);
            }
            // incorrect choice
            else {
                indicators[chosenID].SetActive(true);
                
                // money chosen
                if (moneyID != -1 && moneyID == chosenID) {
                    indicators[chosenID + 6].SetActive(true);
                    hasStolenMoney = true;
                }
            }

            hasChosen = true;
            StartCoroutine(closeDelay());
        }
    }

    public void setParameters(int corrId, Sprite contra, List<Sprite> non, bool hasMoney) {
        // set id and image of contraband object
        correctID = corrId;
        if (correctID != -69)
            sprites[correctID].sprite = contra;
        else
            sprites[0].sprite = contra;

        // set images of non-contraband objects
        for (int i = 0; i < sprites.Length; i++) {
            if (i == correctID)
                continue;

            sprites[i].sprite = non[0];
            non.RemoveAt(0);

            if (hasMoney) moneyID = i;
        }
    }

    // destroy screen when closed
    public void closeScreen() {
        if (correctID != -69) {
            hasChosen = true;
            isCorrect = false;
        }

        Destroy(gameObject);
    }

    IEnumerator closeDelay() {
        yield return new WaitForSecondsRealtime(1f);
        Destroy(gameObject);
    }
}
