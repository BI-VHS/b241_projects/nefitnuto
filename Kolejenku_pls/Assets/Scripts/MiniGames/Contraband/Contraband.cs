using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Contraband : MonoBehaviour {
    [Header("MiniGame settings")]
    [SerializeField] private List<ActivateObject> locations;
    [SerializeField] private Sprite[] goodObjects;
    [SerializeField] private Sprite[] badObjects;
    [SerializeField] private Sprite[] moneyObjects;
    [SerializeField] private int contrabandCount = 3;
    [SerializeField] private int contrabandFound = 0;
    [SerializeField] private GameObject choiceScreen;
    [SerializeField] private int moneyEarned = 0;
    [SerializeField] private int baseEarningPerFind = 100;
    private ContrabandChoice choice;
    [SerializeField] private GameState gameState;
    private bool dontSearch = false;
    private int curLoc = -1;

    [SerializeField] private bool hasStolenMoney = false;
    private bool isDone = false;

    [Header("Timer")]
    public Timer timer;
    [SerializeField] private float secondsLeft = 45f;

    [Header("MiniGame Summary screen")]
    public GameObject endGameScreen;
    public TextMeshProUGUI[] textObjects; // 1st half left side, 2nd half right side
    [SerializeField] private string[] texts;

    void Start() {
        contrabandCount = Random.Range(2, locations.Count);

        int originalCount = locations.Count;
        // n locations, randomly remove (n - contraband count) locations
        for (int i = 0; i < originalCount - contrabandCount; ++i) {
            int id = Random.Range(0, locations.Count);
            locations.RemoveAt(id);
        }

        timer.setTimer(secondsLeft);

        endGameScreen.SetActive(false);
        setTexts();

        // assign game state
        GameObject obj = GameObject.FindGameObjectWithTag("GameState");
        if (obj != null)
            gameState = obj.GetComponent<GameState>();
        else gameState = null;
    }

    void Update() {
        if (isDone) return;

        // minigame finished
        if (!timer.timerOn) {
            calculateEarnings();
            showSummary();
            isDone = true;
            return;
        }

        // check if locations are found
        for (int i = 0; i < locations.Count && !dontSearch; ++i) {
            if (locations[i].isActivated) {
                // remove visited location
                curLoc = i;
                dontSearch = true;
                
                // instantiate choice screen
                GameObject choiceObj = Instantiate(choiceScreen);
                choice = choiceObj.GetComponent<ContrabandChoice>();

                // generate 1st object
                List<Sprite> non = new();
                int rand_1 = Random.Range(0, goodObjects.Length);
                non.Add(goodObjects[rand_1]);

                // generate 2nd object
                // money to be stolen
                bool hasMoney = false;
                if (Random.Range(0, 10) == 6 && !hasStolenMoney) {
                    rand_1 = Random.Range(0, moneyObjects.Length);
                    non.Add(moneyObjects[rand_1]);
                    hasMoney = true;
                }
                // other objects
                else {
                    int rand_2;
                    do {
                        rand_2 = Random.Range(0, badObjects.Length);
                    } while (rand_1 == rand_2);
                    non.Add(goodObjects[rand_2]);
                }

                // no contraband generated
                //if (Random.Range (0, 69) % 13 == 7)
                //    choice.setParameters(-69, goodObjects[Random.Range(0, goodObjects.Length)], non, hasMoney);

                // contraband generated
                //else
                choice.setParameters(Random.Range(0, 3), badObjects[Random.Range(0, badObjects.Length)], non, hasMoney);

                break;
            }
        }

        // remove found location
        if (choice != null && choice.hasChosen) {
            if (choice.isCorrect)
                ++contrabandFound;

            if (choice.hasStolenMoney)
                hasStolenMoney = true;

            locations.RemoveAt(curLoc);
            curLoc = -1;

            choice = null;
            dontSearch = false;
        }

        // end timer if all contrabands have been found
        if (locations.Count == 0)
            timer.timerOn = false;
    }

    void calculateEarnings() {
        float timeMultiplier = 1.0f;
        float timePassed = timer.getTimePassed();

        // calculate multiplier
        if (timePassed > 0)
            timeMultiplier = Mathf.Max(0.5f, 1.5f - (timePassed / 60f));

        // calculate money earned
        if (timePassed < secondsLeft * 0.5f)
            moneyEarned = Mathf.RoundToInt(contrabandFound * baseEarningPerFind * timeMultiplier);
        else
            moneyEarned = contrabandFound * baseEarningPerFind;

        // lose money if no kills
        if (contrabandFound == 0)
            moneyEarned = -Mathf.RoundToInt(contrabandCount * baseEarningPerFind * timeMultiplier);

        // gain some money if stolen from dorm room
        if (hasStolenMoney)
            moneyEarned += Random.Range(baseEarningPerFind / 4, baseEarningPerFind);

        // update bank balance
        if (gameState != null)
            gameState[STATE.MONEY] += moneyEarned;
    }

    void setTexts() {
        // set all left side text objects to inputed text
        for (int i = 0; i < textObjects.Length / 2; i++)
            textObjects[i].text = texts[i];
    }

    void showSummary() {
        float time = timer.getTimePassed();

        float minutes = Mathf.FloorToInt(time / 60);
        float seconds = Mathf.FloorToInt(time % 60);
        float milliseconds = Mathf.FloorToInt(time % 1 * 1000f);

        textObjects[3].text = string.Format("{0:00}:{1:00}.{2:000}", minutes, seconds, milliseconds);
        textObjects[4].text = contrabandFound.ToString() + "/" + contrabandCount.ToString();

        if (moneyEarned > 0)
            textObjects[5].text = "+" + moneyEarned.ToString("N0") + " k�";
        else
            textObjects[5].text = moneyEarned.ToString("N0") + " k�";

        endGameScreen.SetActive(true);
        endGameScreen.GetComponent<Animator>().SetTrigger("EndMiniGame");
    }
}
