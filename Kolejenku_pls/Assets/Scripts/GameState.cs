using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;

using static STATE;

/**
 *  <summary>
 *  Enums used for accessing the accompanied data in GameState class.
 *  </summary>
 */
public enum STATE
{
// game flow
    DAY,
    MONEY,
// game decisions
    SKIBIDI_APPEAR,
    SKIBIDI_ALLOW,
    SKIBIDI_DENY,
    METRO_APPEAR,
    METRO_ALLOW,
    METRO_DENY,
    VSE_APPEAR,
    VSE_ALLOW,
    VSE_DENY,
// statistics
    INDIANS_ALLOWED,
    DISHES_WASHED_COUNT,
    SUZ_APPEAR,
    FINES_RECEIVED // maybe differentiate between types of errors, but this will do for now
}

/**
 *  <summary>
 *  Stores the in-game progress and different statistics used for game flow.
 *  Data is directly accessible using the index operator and enum STATE as indexes.
 *  Also used as player save.
 *  </summary>
 */
public class GameState : MonoBehaviour
{
    [SerializeField]
    public int[] data;

    public GameState()
    {
        int count = 0;
        foreach (STATE state in Enum.GetValues(typeof(STATE)))
        {
            count++;
        }
        data = new int[count];
    }

    public int this[STATE index]
    {
        get => data[Convert.ToInt32(index)];
        set => data[Convert.ToInt32(index)] = value;
    }

    /**
     *  <summary>
     *  Saves the current GameState to persistant storage in "saves/savefile_name.txt".
     *  !!!! TODO fix: only works on linux as of now
     *  </summary>
     *  <param name="savefile_name">Name of the saved file</param>
     *  <returns>True if saved successfully, False otherwise</returns>
     */
    public bool save(string savefile_name)
    {
        // todo add logic for returning False
        
        string savedir_path = Application.persistentDataPath + "/saves/";

        if(!Directory.Exists(savedir_path))
        {
            Directory.CreateDirectory(savedir_path);
        }

        string path =  savedir_path + savefile_name + ".txt";
    
        if(File.Exists(path))
        {
            File.Delete(path);
        }

        using(StreamWriter stream_writer = new StreamWriter(path))
        {
            foreach(var item in data)
            {
                print(item);
                print("string :" + Convert.ToString(item));
                stream_writer.WriteLine(Convert.ToString(item));
            }
        }
        return true;
    }
    /**
     *  <summary>
     *  Loads data from existing file in persistant storage in "saves/savefile_name.txt"
     *  and overwrites the current GameState instance 
     *  !!!! TODO fix: only works on linux as of now
     *  </summary>
     *  <param name="savefile_name">Name of the saved file</param>
     *  <returns>True if loaded successfully, False otherwise</returns>
     */
    public bool load(string savefile_name)
    {
        string savedir_path = Application.persistentDataPath + "/saves/";

        if(!Directory.Exists(savedir_path))
        {
            return false;
        }

        string path = savedir_path + savefile_name + ".txt";

        if(!File.Exists(path))
        {
            return false;
        }

        var newData = data;
        
        using(StreamReader stream_reader = new StreamReader(path))
        {
            for(int i = 0; i < data.Length; ++i)
            {
                string line = stream_reader.ReadLine();

                if(line == null)
                {
                    return false;
                }

                newData[i] = Convert.ToInt32(line);
            }
        }
        data = newData;

        return true;
    }

    // temporary solution
    public void reset()
    {
        for(int i = 0; i < data.Length; ++i)
        {
            data[i] = 0;
        }
    }
}

/*
[CustomEditor(typeof(GameState))]
public class CustomEditorExampleEditor : Editor
{
    SerializedProperty elements;

    private void OnEnable()
    {
        elements = serializedObject.FindProperty("data");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        // Iterate through each element in the list and display its properties on a single line
        for (int i = 0; i < elements.arraySize; i++)
        {
            SerializedProperty element = elements.GetArrayElementAtIndex(i);

            // Get the enum name for the current index
            string state = Enum.GetName(typeof(STATE), i);
            EditorGUILayout.BeginHorizontal();

            // Display element name as label (unchangeable)
            EditorGUILayout.LabelField(state, GUILayout.Width(100));
            
            // Display value field
            element.intValue = EditorGUILayout.IntField(element.intValue);

            EditorGUILayout.EndHorizontal();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
*/