using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Script for managing cleaning movement
/// </summary>
public class Cleaner : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
    /// <summary>
    /// Value, if object should start moving towards the player's mouse
    /// </summary>
    private bool isDragged = false;
    /// <summary>
    /// Movement speed how fast is object moving towards player's mouse
    /// </summary>
    [SerializeField] private float cleanerMovementSpeed;
    /// <summary>
    /// Screen's left down border
    /// </summary>
    [SerializeField] private GameObject leftDownCorner;
    /// <summary>
    /// Screen's top right corner
    /// </summary>
    [SerializeField] private GameObject rightTopCorner;

    private void Update() {
        if (isDragged) {
            Vector3 position = Input.mousePosition;
            // Check if mouse cursor is not out of bounds
            if((position.x < leftDownCorner.transform.position.x || position.y < leftDownCorner.transform.position.y) || 
               (position.x > rightTopCorner.transform.position.x || position.y > rightTopCorner.transform.position.y)) return;
            transform.position = Vector3.MoveTowards(transform.position, position, cleanerMovementSpeed * Time.deltaTime);
        }
    }

    public void OnPointerDown(PointerEventData eventData) {
        if(eventData.pointerCurrentRaycast.gameObject == gameObject) {
            isDragged = true;
        }
    }

    public void OnPointerUp(PointerEventData eventData) {
        isDragged = false;
    }
}
