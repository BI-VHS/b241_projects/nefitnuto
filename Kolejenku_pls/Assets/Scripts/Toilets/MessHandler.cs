using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MessHandler : MonoBehaviour {
    [SerializeField] private GameObject cleaner;
    [SerializeField] private List<Sprite> messInsideToiletBowl;
    [SerializeField] private List<Sprite> messOnWalls;
    [SerializeField] private List<GameObject> doors;
    [SerializeField] private GameObject waypointParent;
    private int waypointsCount;
    private List<bool> waypointsOccupied;


    // Start is called before the first frame update
    void Start() {
        waypointsCount = waypointParent.transform.childCount;
        waypointsOccupied = Enumerable.Repeat(false, waypointsCount).ToList();
        for(int i = 0; i < transform.childCount; i++) {
            int option = Random.Range(0, 2);
            Transform child = transform.GetChild(i);
            if (option == 1) {
                // odd indexes = Toilet Bowl waypoints
                SelectBowlMess(child, option);
            } else {
                // even indexes = wall waypoints
                SelectWallMess(child, option);
            }
            Mess childMess = child.GetComponent<Mess>();
            childMess.ScaleObject();
        }
    }

   private void SelectBowlMess(Transform obj, int value) {
        addTextureOnObject(obj, messInsideToiletBowl);
        MoveMessToPosition(obj, value);
   }

    private void SelectWallMess(Transform obj, int value) {
        addTextureOnObject(obj, messOnWalls);
        MoveMessToPosition(obj, value);
    }

    private Image addTextureOnObject(Transform obj, List<Sprite> images) {
        int texture_index = Random.Range(0, images.Count);
        Image textureImg = obj.GetComponent<Image>();
        textureImg.sprite = images[texture_index];
        return textureImg;
    }

    private void MoveMessToPosition(Transform obj, int value) {
        while (true) {
            int position = 2 * Random.Range(0, waypointsCount / 2) + value;
            if (waypointsOccupied[position] == false) {
                Transform waypoint = waypointParent.transform.GetChild(position);
                obj.position = waypoint.position;
                Mess objMess = obj.gameObject.GetComponent<Mess>();
                objMess.SetCleaner(cleaner);
                OpenDoors cabin = doors[position/2].GetComponent<OpenDoors>();
                cabin.UpdateCabinMess(obj.gameObject);
                waypointsOccupied[position] = true;
                obj.gameObject.SetActive(false);
                return;
            }
        }
    }

}
