using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OpenDoors : MonoBehaviour, IPointerClickHandler {
    [SerializeField] private GameObject openedDoor;
    private List<GameObject> cabinMess = new ();

    public void OnPointerClick(PointerEventData eventData) {
        if(eventData.pointerCurrentRaycast.gameObject == gameObject) {
            AudioSource audio = openedDoor.transform.gameObject.GetComponent<AudioSource>();
            audio.Play();
            openedDoor.SetActive(true);
            foreach(GameObject obj in cabinMess) {
                obj.SetActive(true);
            }
            Destroy(gameObject);
        }
    }

    public void UpdateCabinMess(GameObject cabinMess) {
        this.cabinMess.Add(cabinMess);
    }
}
