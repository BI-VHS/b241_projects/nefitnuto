using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ToiletMiniGame : MonoBehaviour {
    /// <summary>
    /// Main game controller
    /// </summary>
    private GameController controller;
    /// <summary>
    /// List of all the cabin doors in which can mess spawn
    /// </summary>
    [SerializeField] private List<GameObject> doors;
    /// <summary>
    /// All the remaining mess objects in the room
    /// </summary>
    [SerializeField] private List<GameObject> mess;
    /// <summary>
    /// Maximum amount of doors player can open without being penalized
    /// </summary>
    [SerializeField] private int maximumDoorsToOpen;
    /// <summary>
    /// Maximum money sum player can receive from this minigame
    /// </summary>
    [SerializeField] private int startingOutputSum;
    /// <summary>
    /// Price, how much will cost opening next door above the limit
    /// </summary>
    [SerializeField] private int penaltyWhenOpeningDoorsAboveLimit;
    /// <summary>
    /// Timer for next check if door was opened or mess cleaned
    /// </summary>
    [SerializeField] private float timer;
    private float currentTime;
    /// <summary>
    /// Amount of the doors which can be opened
    /// </summary>
    private int amountOfPossibleDoorsToOpen;
    /// <summary>
    /// Game screen which is to be displayed to the player after minigame ends
    /// </summary>
    public GameObject endGameScreen;
    
    // Start is called before the first frame update
    void Start() {
        GameObject firstObject = GameObject.FindGameObjectWithTag("GameController");
        if (firstObject != null) {
            controller = firstObject.GetComponent<GameController>();
        }
        amountOfPossibleDoorsToOpen = doors.Count;
        currentTime = Time.time;
    }

    /// <summary>
    /// Evaluates player's performance during toilet minigame and sends its result to game controller
    /// </summary>
    private int ReturnResultToController() {
        int openedDoors = amountOfPossibleDoorsToOpen - doors.Count;
        int outputSum = startingOutputSum;
        // Free limit was reached
        if (openedDoors > maximumDoorsToOpen) {
            outputSum -= penaltyWhenOpeningDoorsAboveLimit * (openedDoors - maximumDoorsToOpen);
        }
        if (controller != null) {
            controller.game_state[STATE.MONEY] += outputSum;
        }
        return outputSum;
    }

    private void UpdateLists() {
        int doorCounter = doors.Count;
        for (int i = 0; i < doorCounter; i++) {
            if (doors[i] == null) {
                doors.Remove(doors[i--]);
                --doorCounter;
            }
        }
        int messCounter = mess.Count;
        for (int i = 0; i < messCounter; i++) {
            if (mess[i] == null) {
                mess.Remove(mess[i--]);
                --messCounter;
            }
        }
    }

    // Update is called once per frame
    void Update() {
        // Check if all the mess was removed
        if (mess.Count == 0) {
            int gameResult = ReturnResultToController();
            endGameScreen.SetActive(true);
            endGameScreen.GetComponent<Animator>().SetTrigger("EndMiniGame");
            TextMeshProUGUI moneyDisplay = endGameScreen.transform.GetChild(4).GetComponent<TextMeshProUGUI>();
            if (gameResult > 0) {
                moneyDisplay.text = "+" + gameResult.ToString();
                return;
            }
            moneyDisplay.text = gameResult.ToString();
            return;
        }

        if (Time.time > currentTime) {
            currentTime = Time.time + timer;
            this.UpdateLists();
        }

    }
}
