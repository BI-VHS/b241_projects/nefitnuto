using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Create new mess on randomly chosen cabin and when collision with cleaner is detected, removes it
/// </summary>
public class Mess : MonoBehaviour {
    private GameObject cleaner;

    public void ScaleObject() {
        Image img = transform.GetComponent<Image>();
        BoxCollider2D collider = transform.GetComponent<BoxCollider2D>();
        if(collider == null || img == null || img.sprite == null )
            return;
        RectTransform rectTransform = GetComponent<RectTransform>();
        rectTransform.sizeDelta = new Vector2(img.sprite.rect.width, img.sprite.rect.height);
        collider.size = rectTransform.sizeDelta;
    }

    /// <summary>
    /// Detects collision between mess and the cleaner
    /// </summary>
    /// <param name="collision">Object which is colliding with Mess object</param>
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject == cleaner) {
            Destroy(gameObject);
        }
    }

    public void SetCleaner(GameObject cleaner) {
        this.cleaner = cleaner;
    }

}
