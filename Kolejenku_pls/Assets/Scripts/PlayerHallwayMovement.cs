using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.Mathematics;
using UnityEngine;

/// <summary>
/// Basic player movement script to handle first person movement on x coordinate
/// </summary>
public class PlayerHallwayMovement : MonoBehaviour {
    public float speed;
    public float maximumDistanceFromStartingPoint;
    private Vector2 startingPosition;
    private float direction;

    /// <summary>
    /// Gets Init player position
    /// </summary>
    void Start () {
        startingPosition = transform.position;
    }

    /// <summary>
    /// Checks if player input is valid and if so, changes player position according to given keyboard input
    /// </summary>
    void Update() {
        direction = Input.GetAxis("Horizontal");
        float newPossiblePosition = transform.position.x + (direction * speed);
        if (direction > 0f) {
            if(MoveRight(newPossiblePosition)) {
                transform.Translate(new Vector2(direction * speed, 0));
                return;
            }
            Vector2 borderRightPosition = new Vector2(startingPosition.x + maximumDistanceFromStartingPoint, startingPosition.y);
            transform.position = borderRightPosition;

        } else if(direction < 0f ) {
            if(MoveLeft(newPossiblePosition)) {
                transform.Translate(new Vector2(direction * speed, 0));
                return;
            }
            Vector2 borderLeftPosition = new Vector2(startingPosition.x - maximumDistanceFromStartingPoint, startingPosition.y);
            transform.position = borderLeftPosition;
        }
    }

    /// <summary>
    /// Checks if next move would not get player out of the given bounds
    /// </summary>
    /// <param name="possiblePosition">Position where is player trying to move to.</param>
    /// <returns> Information if mobing to the right is possible </returns>
    private bool MoveRight(float possiblePosition) {
        return possiblePosition < (startingPosition.x + maximumDistanceFromStartingPoint);
    }
    /// <summary>
    /// Analogical to MoveRight method
    /// </summary>
    /// <param name="possiblePosition">Position where is player trying to move to.</param>
    /// <returns> Information if moving to the left is possible </returns>
    private bool MoveLeft(float possiblePosition) {
        return possiblePosition > (startingPosition.x - maximumDistanceFromStartingPoint);
    }

}
