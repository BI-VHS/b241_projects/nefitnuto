using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Specific instance within the queue
/// </summary>
public class QueuedPerson : MonoBehaviour {
    private Animator animator;
    /// <summary>
    /// Waypoint index in the list, so Queue handler can easily read position and set next waypoint
    /// </summary>
    private int waypointIndex;
    /// <summary>
    /// Position, where is person currently heading
    /// </summary>
    private Vector3 currentWaypoint;
   

    // Start is called before the first frame update
    void Start() {
        animator = GetComponent<Animator>();
        animator.SetTrigger("Move");
    }  

    /// <summary>
    /// Swaps between idle and walking animation
    /// </summary>
    public void ChangeMovementAnimation() {
        animator.SetTrigger("Move");
    }

    /// <summary>
    /// Getter for starting person position
    /// </summary>
    /// <returns> positon of first waypoint from which is starting position being calculated </returns>
    public Vector3 GetNewStartingPoint() {
        return currentWaypoint;
    }

    public Vector3 GetCurrentWaypoint() { return currentWaypoint; }
    public int GetWaypointIndex() {  return waypointIndex; }
    public void SetWaypointIndex(int waypointIndex) { this.waypointIndex = waypointIndex; }
    public Animator GetAnimator() { return animator; }
    public void SetWaypoint(Vector3 waypoint) {  currentWaypoint = waypoint; }

}
