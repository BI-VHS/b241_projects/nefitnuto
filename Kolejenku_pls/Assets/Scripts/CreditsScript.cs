using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple script for swapping between main menu and credits menu
/// </summary>
public class CreditsScript : MonoBehaviour {
    [SerializeField] private GameObject creditsBackground;
    [SerializeField] private GameObject returnButton;
    [SerializeField] private GameObject oldButtons;

    public void ShowCredits() {
        oldButtons.SetActive(false);
        creditsBackground.SetActive(true);
        returnButton.SetActive(true);
    }

    public void HideCredits() {
        creditsBackground.SetActive(false);
        returnButton.SetActive(false);
        oldButtons.SetActive(true);
    }

}
