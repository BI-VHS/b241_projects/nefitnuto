using TMPro;
using UnityEngine;

public class DailySummary : MonoBehaviour {
    [Header("Text for daily summary screen")]
    [SerializeField] private DayStatistics statistics;
    public TextMeshProUGUI[] textObjects;
    [SerializeField] private GameState gameState;

    // sets correct data into text fileds in daily summary screen
    void Start() {
        GameObject obj  = GameObject.FindGameObjectWithTag("GameState");
        GameObject obj2 = GameObject.Find("DayStatistics");

        if (obj != null && obj2 != null) {
            gameState  = obj.GetComponent<GameState>();
            statistics = obj2.GetComponent<DayStatistics>();
            
            textObjects[0].text = "Shrnutí " + gameState[STATE.DAY].ToString() + ". dne";
            textObjects[1].text = statistics.person_count.ToString();
            textObjects[2].text = statistics.fines_count.ToString();
            textObjects[3].text = statistics.money_made.ToString();
            textObjects[4].text = gameState[STATE.MONEY].ToString();
        }
    }
}
