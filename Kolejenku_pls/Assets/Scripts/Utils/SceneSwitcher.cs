using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour {
    [Header("Fade animation between scenes")]
    public Animator sceneFade;
    public float transitionDuration = 1.0f;

    // methods used for loading all scenes in the game

    public void LoadMainMenu() {
        StartCoroutine(LoadLevel("Main_menu"));
    }
    
    public void LoadEntrance() {
        StartCoroutine(LoadLevel("Entrance"));
    }
    
    public void LoadIntroduction() {
        StartCoroutine(LoadLevel("Intro"));
    }

    public void LoadHallway() {
        StartCoroutine(LoadLevel("Hallway"));
    }

    public void LoadDormRoomStenice() {
        StartCoroutine(LoadLevel("DormRoom Stenice"));
    }

    public void LoadDormRoomContraband() {
        StartCoroutine(LoadLevel("DormRoom Contraband"));
    }

    public void LoadKitchen() {
        StartCoroutine(LoadLevel("Kitchen"));
    }
    
    public void LoadShowers() {
        StartCoroutine(LoadLevel("Showers"));
    }
    
    public void LoadToilets() {
        StartCoroutine(LoadLevel("Toilets"));
    }

    public void LoadStoryStenice() {
        StartCoroutine(LoadLevel("StoryStenice"));
    }

    public void LoadStoryKitchen()
    {
        StartCoroutine(LoadLevel("StoryKuchyn"));
    }

    public void LoadStoryToilets() {
        StartCoroutine(LoadLevel("StoryToilets"));
    }

    public IEnumerator LoadLevel(string name) {
        sceneFade.SetTrigger("Fade");

        yield return new WaitForSeconds(transitionDuration);

        SceneManager.LoadScene(name);
    }
}
