using UnityEngine;
using UnityEngine.EventSystems;

public class ActivateObject : MonoBehaviour, IPointerClickHandler {
    [SerializeField] private GameObject activate;
    public bool isActivated = false;

    // clicking on object with this script attached will set active sprite of activated object
    public void OnPointerClick(PointerEventData eventData) {
        if (!isActivated) {
            activate.SetActive(!activate.activeInHierarchy);
            isActivated = true;
        }
    }
}
