using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draggable : MonoBehaviour
{
    bool dragging = false;
    Vector3 offset = Vector3.zero;
    Camera mainCamera;
    public BoxCollider play_area;

    void Start()
    {
        mainCamera = Camera.main;

        var x = GameObject.Find("Play Area");

        if(x)
        {
            play_area = x.GetComponent<BoxCollider>();
        }
    }

    void Update()
    {   
        if(!dragging)
        {
            return;
        }
        
        Vector3 mouse_pos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        Vector3 target_pos = mouse_pos + offset;

        // Get the screen bounds
        Vector3 screenBounds = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, mainCamera.transform.position.z));
        
        // Clamp the target position to the screen bounds
        if(!play_area)
        {
            target_pos.x = Mathf.Clamp(target_pos.x, -screenBounds.x, screenBounds.x);
            target_pos.y = Mathf.Clamp(target_pos.y, -screenBounds.y, screenBounds.y);
        }
        else
        {
            Bounds bounds = play_area.bounds;
            target_pos.x = Mathf.Clamp(target_pos.x, bounds.min.x, bounds.max.x);
            target_pos.y = Mathf.Clamp(target_pos.y, bounds.min.y, bounds.max.y);
        }

        transform.position = new Vector3(target_pos.x, target_pos.y, transform.position.z);
    }

    void OnMouseDown()
    {
        Vector3 mouse_pos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        // offset prevents the objects center jumping to the cursor 
        offset = transform.position - new Vector3(mouse_pos.x, mouse_pos.y, transform.position.z);
        dragging = true;
    }

    void OnMouseUp()
    {
        dragging = false;
    }

    public bool isDragging()
    {
        return dragging;
    }
}
