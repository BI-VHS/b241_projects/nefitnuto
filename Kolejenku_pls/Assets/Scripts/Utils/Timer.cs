using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour {
    public float secondsLeft;
    private float originalTime;
    public bool timerOn = true;

    public TextMeshProUGUI timerText;

    void Start() {
        timerOn = true;
        originalTime = secondsLeft;
    }

    void Update() {
        if (timerOn) {
            if (secondsLeft > 0) {
                secondsLeft -= Time.deltaTime;
                updateTimer(secondsLeft);
            }
            else {
                secondsLeft = 0;
                timerOn = false;
                timerText.text = "00:00.000";
            }
        }
    }

    void updateTimer(float currentTime) {
        currentTime += 1;

        float minutes = Mathf.FloorToInt(currentTime / 60);
        float seconds = Mathf.FloorToInt(currentTime % 60);
        float milliseconds = Mathf.FloorToInt(currentTime % 1 * 1000f);

        timerText.text = string.Format("{0:00}:{1:00}.{2:000}", minutes, seconds, milliseconds);
    }

    public void setTimer(float t) => secondsLeft = originalTime = t;

    public float getTimePassed() => originalTime - secondsLeft;
}
