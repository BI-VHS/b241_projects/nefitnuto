using UnityEngine;

public class SceneVariation : MonoBehaviour {
    [Header("Randomly spawned room decoration")]
    [SerializeField] private GameObject[] roomDecoGroups;
    [SerializeField] private int cleanProbability = 1;

    [Header("Object color variation")]
    [SerializeField] private SpriteRenderer[] objToBeColored;
    [SerializeField] private Color[] colors;

    [Header("If other objects need different color variations")]
    [SerializeField] private SpriteRenderer[] objToBeColored2;
    [SerializeField] private Color[] colors2;

    void Start() {
        // randomly spawn (or don't) room decoration
        foreach (GameObject roomGroup in roomDecoGroups) {
            int objectID = Random.Range(0, roomGroup.transform.childCount + cleanProbability);

            if (objectID > roomGroup.transform.childCount)
                roomGroup.SetActive(false);

            for (int i = 0; i < roomGroup.transform.childCount; i++) {
                if (i == objectID) {
                    roomGroup.transform.GetChild(i).gameObject.SetActive(true);
                    break;
                }
            }
        }

        // randomly select a given color and overlay on object
        foreach (SpriteRenderer obj in objToBeColored) {
            int objectID = Random.Range(0, colors.Length);
            colors[objectID].a = 1;
            obj.color = colors[objectID];
        }

        // randomly select a given color2 and overlay on object2
        if (objToBeColored2 == null || colors2 == null) return;
        foreach (SpriteRenderer obj in objToBeColored2) {
            int objectID = Random.Range(0, colors2.Length);
            colors2[objectID].a = 1;
            obj.color = colors2[objectID];
        }
    }
}
