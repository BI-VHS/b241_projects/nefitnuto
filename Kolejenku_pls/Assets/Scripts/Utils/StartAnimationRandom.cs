using UnityEngine;

public class StartAnimationRandom : MonoBehaviour {
    [SerializeField] private Animator animator;

    void Start() {
        // start animation at random frame
        var state = animator.GetCurrentAnimatorStateInfo(0);
        animator.Play(state.fullPathHash, 0, Random.Range(0f, 1f));
    }
}
