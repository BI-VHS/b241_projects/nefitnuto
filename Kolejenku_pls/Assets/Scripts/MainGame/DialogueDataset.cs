using System;

[Serializable]
public class DialogueDataset
{
    public string[] register_entry;
    public string[] open_entry;
    public string[] allow;
    public string[] deny;
    public string[] wrong;
    public string[] wrong_id;
    public string[] wrong_name;
    public string[] wrong_blok;
    public string[] wrong_photo;
    public string[] wrong_stamp;
    public string[] wrong_date;
}

[Serializable]
public class UniqueDialogueDataset
{
    public Dialogue[] register_entry;
    public Dialogue[] open_entry;
    public Dialogue[] allow;
    public Dialogue[] deny;
    public Dialogue[] wrong;
    public Dialogue[] wrong_id;
    public Dialogue[] wrong_name;
    public Dialogue[] wrong_blok;
    public Dialogue[] wrong_photo;
    public Dialogue[] wrong_stamp;
    public Dialogue[] wrong_date;

    [Serializable]
    public class Dialogue
    {
        public string text;
        public bool is_player;
    }
}
