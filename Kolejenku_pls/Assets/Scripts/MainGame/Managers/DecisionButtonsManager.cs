using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DecisionButtonsManager
{
    // yes it could generic, but i dont see the reason
    private Button allow_button = GameObject.Find("Button-AllowEnter").GetComponent<Button>();
    private Button deny_button = GameObject.Find("Button - Deny Enter").GetComponent<Button>();
    private bool active = false;

    public DecisionButtonsManager()
    {
        allow_button.interactable = false;
        deny_button.interactable = false;
    }

    public void SetActive(bool value)
    {
        if(value != active)
        {
            allow_button.interactable = value;
            deny_button.interactable = value;
            active = value;
        }
    }
}
