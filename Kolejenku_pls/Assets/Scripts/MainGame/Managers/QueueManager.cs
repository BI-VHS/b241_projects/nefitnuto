using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;
using static STATE;

public class QueueManager
{
    private PeopleAtDesks people_at_desks;
    private PersonGenerator person_generator;
    public PaperManager paper_manager;
    private GameState game_state;
    private Days days;

    public QueueManager(GameState game_state, Days days)
    {
        this.game_state = game_state;
        this.days = days;

        string person_json = Resources.Load<TextAsset>("person_dataset").text;
        string school_json = Resources.Load<TextAsset>("school_dataset").text;

        person_generator = new PersonGenerator(person_json, school_json);

        people_at_desks = GameObject.Find("PersonHandler").GetComponent<PeopleAtDesks>();

        paper_manager = new PaperManager(game_state, days);
    }

    public void CreateQueue()
    {
        int people_generated = 5;

        List<PersonToCheck> uniques = AddUniques();

        people_generated += uniques.Count * 3;

        List<PersonToCheck> people_to_add = generatePeople(people_generated);

        if(days.isRuleActive(game_state[DAY], RulesEnum.UniqueCharacters.SUZ))
        {            
            GameObject suz_prefab = Resources.Load<GameObject>("Unique Characters/SUZ/Suz");
            PersonToCheck suz = GameObject.Instantiate(suz_prefab).GetComponent<PersonToCheck>();
            suz.gameObject.transform.position = people_at_desks.personToCheckWaypoints[0].transform.position;
            people_to_add.Insert(0, suz);
        }

        foreach(var uni in uniques)
        {
            int random_int = UnityEngine.Random.Range(1, people_generated);
            people_to_add.Insert(random_int, uni);
            if(uni.gameObject.name == "VSE(Clone)" && game_state[VSE_APPEAR] == 2)
            {
                GameObject bomb_expert_prefab = Resources.Load<GameObject>("Unique Characters/BombExpert/BombExpert");
                PersonToCheck bomb_expert = GameObject.Instantiate(bomb_expert_prefab).GetComponent<PersonToCheck>();
                bomb_expert.gameObject.transform.position = people_at_desks.personToCheckWaypoints[0].transform.position;
                people_to_add.Insert(random_int + 1, bomb_expert);
            }
        }

        foreach(var person in people_to_add)
        {
            people_at_desks.personsAtQueue.Enqueue(person);        
        }
    }

    public void AddPeople(int number)
    {
        var new_people = generatePeople(number);
        foreach(var person in new_people)
        {
            people_at_desks.personsAtQueue.Enqueue(person);
        }
    }

    private void checkForFloorRules(PersonToCheck person)
    {
        string room_number = Convert.ToString(person.roomNumber);

        if(room_number.Length < 3)
        {
            if(days.isRuleActive(game_state[DAY], RulesEnum.Floor.F0))
            {
                person.illegal = true;
                person.paperLegality.has_fake_blok_number = true;
            }

            return;
        }
        
        char room_floor = room_number[0];

        RulesEnum.Floor floor = (RulesEnum.Floor)Enum.Parse(typeof(RulesEnum.Floor), "F" + room_floor);

        if(days.isRuleActive(game_state[DAY], floor))
        {
            person.illegal = true;
            person.paperLegality.has_fake_blok_number = true;
        }
    }

    private List<PersonToCheck> generatePeople(int number)
    {
        List<PersonToCheck> ret = new List<PersonToCheck>();
        ret.Capacity = number;
        for(int i = 0; i < number; ++i)
        {
            // generate character
            PersonToCheck person = person_generator.Create().GetComponent<PersonToCheck>();
            person.gameObject.transform.localScale *= 0.75f;
            person.gameObject.transform.position = people_at_desks.personToCheckWaypoints[0].transform.position;
            person.gameObject.GetComponent<SortingGroup>().sortingOrder = 0;
            
            if(days.isRuleActive(game_state[DAY], RulesEnum.PersonQuery.REGISTER))
            {
                person.registered = UnityEngine.Random.Range(0, 1f) > 0.5f;

                if(!person.registered)
                if(!days.isRuleActive(game_state[DAY], RulesEnum.AllowQuery.REGISTER))
                {
                    person.illegal = true;
                }
            }

            if(person.registered)
            if(!days.isRuleActive(game_state[DAY], RulesEnum.AllowQuery.OPEN))
            {
                person.illegal = true;
            }

            string blok = "B" + Convert.ToString(person.blockNumber);

            if(UnityEngine.Random.Range(0, 1f) > 0.8f)
            {
                person.illegal = true;
            }
            paper_manager.addDocuments(person);

            if(person.registered)
            {
                RulesEnum.Blok blok_rule;
                Enum.TryParse(blok, out blok_rule);
                if(!days.isRuleActive(game_state[DAY], blok_rule))
                {
                    person.illegal = true;
                    person.paperLegality.has_fake_blok_number = true;
                }

                checkForFloorRules(person);
            }
            RulesEnum.Nationality nat;
            Enum.TryParse(person.nationality.ToUpper(), out nat);
            if(days.isRuleActive(game_state[DAY], nat))
            {
                person.illegal = true;
            }

            ret.Add(person);
        }

        return ret;
    }

    // <summary>Adds unique characters</summary>
    private List<PersonToCheck> AddUniques()
    {
        List<GameObject> prefabs = new List<GameObject>();

        if(days.isRuleActive(game_state[DAY], RulesEnum.UniqueCharacters.SKIBIDI))
        {            
            if(game_state[SKIBIDI_APPEAR] == 0)
            {
                prefabs.Add(Resources.Load<GameObject>("Unique Characters/Skibidi/0/Skibidi"));
            }
            else if(game_state[SKIBIDI_APPEAR] == 1 && game_state[SKIBIDI_DENY] == 1)
            {
                prefabs.Add(Resources.Load<GameObject>("Unique Characters/Skibidi/1/Skibidi"));
            }         
            ++game_state[SKIBIDI_APPEAR];
        }
        if(days.isRuleActive(game_state[DAY], RulesEnum.UniqueCharacters.VSE))
        {   
            if(game_state[VSE_APPEAR] == 0)
            {
                prefabs.Add(Resources.Load<GameObject>("Unique Characters/VSE/0/VSE"));
            }
            else if(game_state[VSE_APPEAR] == 1 && game_state[VSE_DENY] == 1)
            {
                prefabs.Add(Resources.Load<GameObject>("Unique Characters/VSE/1/VSE"));
            }         
            ++game_state[VSE_APPEAR];
        }
        if(days.isRuleActive(game_state[DAY], RulesEnum.UniqueCharacters.METRO))
        {
            prefabs.Add(Resources.Load<GameObject>("Unique Characters/Metro/Metro"));
        }

        List<PersonToCheck> uniques = new List<PersonToCheck>();

        foreach(var prefab in prefabs)
        {
            PersonToCheck unique = GameObject.Instantiate(prefab).GetComponent<PersonToCheck>();
            // spawn at right position
            unique.gameObject.transform.position = people_at_desks.personToCheckWaypoints[0].transform.position;
            
            uniques.Add(unique);
        }

        return uniques;
    }
}
