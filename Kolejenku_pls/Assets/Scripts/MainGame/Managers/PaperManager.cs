using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static STATE;
public class PaperManager
{
    private GameState game_state;
    private Days days;

    public PaperManager(GameState game_state, Days days)
    {
        this.game_state = game_state;
        this.days = days;
    }

    public void addDocuments(PersonToCheck person)
    {
        if(person.registered)
        {
            person.documents = CreateOpenDocuments(person);
        }
        else
        {
            person.documents = CreateRegisterDocuments(person);
        }
    }

    private List<Paper> CreateOpenDocuments(PersonToCheck person)
    {
        var ret = new List<Paper>();

        if(days.isRuleActive(game_state[DAY], RulesEnum.Paper.KOLEJENKA))
        {
            ret.Add(new Kolejenka(person));
        }

        if(days.isRuleActive(game_state[DAY], RulesEnum.SpecialCases.CROSSCHECK_KOLEJENKA))
        {
            if(days.isRuleActive(game_state[DAY], RulesEnum.Paper.ID))
            {
                switch(person.nationality)
                {
                case "Czech":
                    ret.Add(new IdCardCZ(person));
                    break;
                case "Slovak":
                    ret.Add(new IdCardSK(person));
                    break;
                default:
                    ret.Add(new Passport(person));
                    break;
                }
            }
        }

        return ret;
    }

    private List<Paper> CreateRegisterDocuments(PersonToCheck person)
    {
        var ret = new List<Paper>();

        string name = "";
        if(days.isRuleActive(game_state[DAY], RulesEnum.Paper.ID))
        {
            IdCard idc = null;
            Passport pass = null;
            switch(person.nationality)
            {
            case "Czech":
                ret.Add(idc = new IdCardCZ(person));
                break;
            case "Slovak":
                ret.Add(idc = new IdCardCZ(person));
                break;
            default:
                ret.Add(pass = new Passport(person));
                break;
            }
            if(idc != null)
            {
                name = idc.name;
            }
            else
            {
                name = pass.name;
            }
            var split_name = name.Split('\n');
            name = "";
            for(int i = 0; i < split_name.Length; ++i)
            {
                name += split_name[i];
                if(i + 1 != split_name.Length)
                {
                    if(i % 2 == 0)
                    {
                        name += '\n';
                    }
                    else
                    {
                        name += ' ';
                    }
                }
            }
        }
        if(days.isRuleActive(game_state[DAY], RulesEnum.Paper.COS))
        {
            ret.Add(new CoS(person));
        }

        Kolejenka new_kolejenka = new Kolejenka(person);
        new_kolejenka.name = name;
        new_kolejenka.block_number = 13;
        new_kolejenka.school = person.school;
        ret.Add(new_kolejenka);

        return ret;
    }
}
