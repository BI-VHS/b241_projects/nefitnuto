using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/**
 *  <summary>
 *  Czech ID Card
 *  </summary>
 */
[CreateAssetMenu(fileName = "New IdCardCZ", menuName = "New Paper/ID Card/Czech")]
public class IdCardCZ : IdCard
{
    /**
     *  <summary>
     *  Creates Czech ID card with information from `person`
     *  </summary>
     *  <param name="person"> Owner of the card </param>
     */
    public IdCardCZ(PersonToCheck person)
        : base(person)
    {
        prefab = GameObject.Find("Resources z wishe").GetComponent<PrefabList>().get(PrefabName.ID_CZ);
    }
}