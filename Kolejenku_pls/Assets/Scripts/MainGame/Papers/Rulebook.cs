using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Rulebook : Paper
{
    public string rules;
    
    public Rulebook(string rules)
    {        
        prefab = GameObject.Find("Resources z wishe").GetComponent<PrefabList>().get(PrefabName.RULEBOOK);

        this.rules = rules;
    }
    
    public override GameObject Create()
    {
        game_object = GameObject.Instantiate(prefab, spawn);    

        TextMeshProUGUI[] text_fields = game_object.GetComponentsInChildren<TextMeshProUGUI>();

        foreach(var text in text_fields)
        {
            switch(text.gameObject.name)
            {
            case("Rules"):
                text.text = rules;
                break;
            default:
                throw new Exception("Unknown text field: " + text.gameObject.name);
            }
        }

        return game_object;
    }
}
