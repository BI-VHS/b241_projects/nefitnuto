using System; 
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

/**
 *  <summary>
 *  Stores different stamps. Differentiates between valid and invalid stamps.
 *  Needs to be filled manually in editor.
 *  </summary>
 */
public class StampList : MonoBehaviour
{
    public Sprite kolejenka_default;
    [Serialize]
    public Sprite[] kolejenka_valid;
    [Serialize]
    public Sprite[] kolejenka_invalid;

    // TODO rename i guess
    
    /**
     *  <summary>
     *  Returns random valid/invalid stamp for specific paper. 
     *  </summary>
     *  <param name="prefab_name">Type of paper</param>
     *  <param name="valid">Specifies legality of returned stamp</param>
     *  <returns>Stamp Sprite</returns>
     */
    public Sprite get(PrefabName prefab_name, bool valid)
    {
        System.Random random = new System.Random();

        switch(prefab_name)
        {
        case(PrefabName.KOLEJENKA):
            if(valid)
            {
                return kolejenka_valid[random.Next(0, kolejenka_valid.Length - 1)];
            }
            return kolejenka_invalid[random.Next(0, kolejenka_invalid.Length - 1)];
        default:
            break;
        }

        return kolejenka_default;
    }
}
