using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UIElements;

[CreateAssetMenu(fileName = "New Fine", menuName = "New Paper/Fine")]
public class Fine : Paper
{
    public string message;
    
    public Fine(string message)
    {
        spawn = GameObject.Find("Fine Spawn Point").transform;        
        
        prefab = GameObject.Find("Resources z wishe").GetComponent<PrefabList>().get(PrefabName.FINE);

        this.message = message;
    }
    
    public override GameObject Create()
    {
        game_object = base.Create();    

        TextMeshProUGUI[] text_fields = game_object.GetComponentsInChildren<TextMeshProUGUI>();

        foreach(var text in text_fields)
        {
            switch(text.gameObject.name)
            {
            case("Message"):
                text.text = message;
                break;
            default:
                throw new Exception("Unknown text field: " + text.gameObject.name);
            }
        }

        return game_object;
    }
}
