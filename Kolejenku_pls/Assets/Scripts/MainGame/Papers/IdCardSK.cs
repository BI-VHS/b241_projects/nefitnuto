using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/**
 *  <summary>
 *  Slovak ID Card
 *  </summary>
 */
[CreateAssetMenu(fileName = "New IdCardSK", menuName = "New Paper/ID Card/Slovak")]
public class IdCardSK : IdCard
{
    /**
     *  <summary>
     *  Creates Slovak ID card with information from `person`
     *  </summary>
     *  <param name="person"> Owner of the card </param>
     */
    public IdCardSK(PersonToCheck person)
        : base(person)
    {
        prefab = GameObject.Find("Resources z wishe").GetComponent<PrefabList>().get(PrefabName.ID_SK);
    }
}
