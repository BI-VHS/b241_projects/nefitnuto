using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;

/**
 *  <summary>
 *  Base class for all papers. Use only for inheritance and abstraction.
 *  </summary>
 */
[Serializable]
public class Paper : ScriptableObject
{
    public GameObject prefab;
    public Transform spawn;

    public GameObject game_object; // <summary> holds reference to its own GameObject when it exists </ summary>
    public GameObject photo;
    public PersonToCheck person;

    public new string name;

    public Paper()
    {
    }

    protected void createPhoto()
    {
        photo = GameObject.Instantiate(person.gameObject);
        photo.transform.localScale = new Vector3(0.21f, 0.25294f, 0.20026f);
        photo.transform.parent = game_object.transform.GetChild(2).transform;
        photo.GetComponent<SortingGroup>().sortingOrder = 4;

        Destroy(photo.GetComponent<PersonToCheck>());
        Destroy(photo.GetComponent<PersonToCheck>());
        Destroy(photo.GetComponent<SceneVariation>());
    }
    /**
     *  <summary>
     *  Instantiates its GameObject from prefab and fills in its own information
     *  </summary>
     *  <returns>the new GameObject reference</returns>
     */
    public virtual GameObject Create()
    {
        if(spawn == null)
        {
            spawn = GameObject.Find("Papers Spawn Point").transform;        
        }

        return game_object = GameObject.Instantiate(prefab, spawn);
    }
}
