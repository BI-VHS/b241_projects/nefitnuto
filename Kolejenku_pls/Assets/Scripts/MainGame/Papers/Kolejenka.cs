using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[CreateAssetMenu(fileName = "New Kolejenka", menuName = "New Paper/Kolejenka")]
public class Kolejenka : Paper
{
    public int block_number;
    public int room_number;
    public string school;
    public bool fakeStamp = false;

    public Kolejenka(PersonToCheck person)
    {
        this.person = person;

        prefab = GameObject.Find("Resources z wishe").GetComponent<PrefabList>().get(PrefabName.KOLEJENKA);

        if(person.firstName.Split(' ').Length > 1)
        {
            name = person.firstName + '\n' + person.lastName;
        }
        else
        {
            name = person.firstName + " " + person.lastName;
        }
        block_number = person.blockNumber;
        room_number = person.roomNumber;
        school = person.school;
        
        if(person.illegal && !person.hasFake)
        {
            // if(UnityEngine.Random.Range(0, 1f) < 0.5)
            // {
            //     name = "Lauren McLovin'";
            //     person.paperLegality.has_fake_name = true;
            // }
            // else
            // {
                fakeStamp = true;
                person.paperLegality.has_fake_stamp = true;
            // }
            person.hasFake = true;
        }
    }

    public override GameObject Create()
    {
        game_object = base.Create();

        TextMeshProUGUI[] text_fields = game_object.GetComponentsInChildren<TextMeshProUGUI>();

        foreach(var text in text_fields)
        {
            switch(text.gameObject.name)
            {
            case("Name"):
                text.text = name;
                break;
            case("Block Number"):
                text.text = Convert.ToString(block_number);
                break;
            case("Room Number"):
                text.text = Convert.ToString(room_number);
                break;
            case("University and Faculty"):
                text.text = school;
                break;
            default:
                break;
            }
        }

        SpriteRenderer[] sprites = game_object.GetComponentsInChildren<SpriteRenderer>();

        GameObject stamps_types = game_object.transform.GetChild(2).GetChild(1).gameObject;
        GameObject stamps = stamps_types.transform.GetChild(Convert.ToInt32(fakeStamp)).gameObject;

        int random_int = UnityEngine.Random.Range(0, stamps.transform.childCount);

        stamps.transform.GetChild(random_int).gameObject.SetActive(true);

        createPhoto();
        photo.transform.localPosition = new Vector3(-0.546f, 0.688f, 0);

        game_object.GetComponent<PaperSpriteChanger>().switchToSmallSprite();

        return game_object;
    }
}
