using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

using static PaperSpriteChanger.SpriteType;

public class PaperGiving: MonoBehaviour
{
    public BoxCollider return_area;
    public BoxCollider2D paper_area;

    public PaperSpriteChanger paperSpriteChanger;
    public Draggable draggable;

    public GameObject givecon;
    public int givecon_height = 1;

    // Start is called before the first frame update
    void Start()
    {
        return_area = GameObject.Find("Paper Give Area").GetComponent<BoxCollider>();
        paper_area = GetComponent<BoxCollider2D>();

        paperSpriteChanger = GetComponent<PaperSpriteChanger>();

        draggable = GetComponent<Draggable>();
    }

    // Update is called once per frame
    void Update()
    {
        if(paperSpriteChanger.spriteType == FULL || !paper_area.bounds.Intersects(return_area.bounds) || !draggable.isDragging())
        {
            if(givecon.activeInHierarchy)
            {
                givecon.SetActive(false);
            }
            return;
        }

        if(!givecon.activeInHierarchy)
        {
            givecon.SetActive(true);
        }

        givecon.transform.position = transform.position + Vector3.up * givecon_height;
    }

    void OnMouseUp()
    {
        if(paperSpriteChanger == null || paper_area == null)
        {
            return;
        }

        if(paperSpriteChanger.spriteType == SMALL && paper_area.bounds.Intersects(return_area.bounds))
        {
            Destroy(gameObject);
            givecon.SetActive(false);
        }
    }
}
