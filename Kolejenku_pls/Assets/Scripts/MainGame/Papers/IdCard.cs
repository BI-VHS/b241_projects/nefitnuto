using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/**
 *  <summary>
 *  Base class for IdCards. Use only for inheritance and abstraction.
 *  </summary>
 */
public class IdCard : Paper
{
    public string id;
    public string expiration;
    
    public IdCard(PersonToCheck person)
    {
        this.person = person;

        name = person.firstName + "\n" + person.lastName; // needs this format for correct rendering in scene
                                                            // without saving the first and last name separately
                                                            // and also checking of the name checks the whole name
     
        // randomly generate expiration date

        int year; 
        int month;
        int day;

        year = UnityEngine.Random.Range(2025, 2035 + 1);
        id = person.id;

        if(person.illegal && !person.hasFake)
        {
            float random = UnityEngine.Random.Range(0, 1f);
            if(random < 0.33f)
            {
                year = UnityEngine.Random.Range(2020, 2023 + 1);

                person.paperLegality.has_fake_date = true;
            }
            else if(random < 0.66f)
            {
                do
                {
                    id = "";
                    for(int i = 0; i < 8; ++i)
                    {
                        if(UnityEngine.Random.Range(0, 2) == 1)
                        {
                            id += Convert.ToChar(UnityEngine.Random.Range('A', 'Z'));
                        }
                        else
                        {
                            id += Convert.ToString(UnityEngine.Random.Range(0, 9 + 1));
                        }
                        if(i == 3)
                        {
                            id += '-';
                        }
                    }
                }
                while(id == person.id);

                person.paperLegality.has_fake_id = true;
            }
            else if(random < 1f)
            {
                name = person.fake_firstName + "\n" + person.fake_lastName;
            
                person.paperLegality.has_fake_name = true;
            }
            person.hasFake = true;
        }
        month = UnityEngine.Random.Range(1, 12 + 1);
        day = UnityEngine.Random.Range(1, DateTime.DaysInMonth(year, month) + 1);

        expiration = day + ". " + month + ". " + year;
    }
    
    public override GameObject Create()
    {
        game_object = base.Create();    

        TextMeshProUGUI[] text_fields = game_object.GetComponentsInChildren<TextMeshProUGUI>();

        foreach(var text in text_fields)
        {
            switch(text.gameObject.name)
            {
            case("Name"):
                text.text = name;
                break;
            case("Expiration"):
                text.text = expiration;
                break;
            case("ID Number"):
                text.text = Convert.ToString(id);
                break;
            default:
                throw new Exception("Unknown text field: " + text.gameObject.name);
            }
        }

        SpriteRenderer[] sprites = game_object.GetComponentsInChildren<SpriteRenderer>();

        createPhoto();
        photo.transform.localPosition = new Vector3(-1.101f, -0.243f, 0);

        game_object.GetComponent<PaperSpriteChanger>().switchToSmallSprite();

        return game_object;
    }
}
