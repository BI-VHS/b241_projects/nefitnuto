using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class ClickToInvastigate : MonoBehaviour
{
    public Light2D lights;

    void OnMouseDown()
    {
        lights.enabled = !lights.enabled;
    }
}
