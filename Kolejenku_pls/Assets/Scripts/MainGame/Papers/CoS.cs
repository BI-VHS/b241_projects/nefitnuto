using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[CreateAssetMenu(fileName = "New CoS", menuName = "New Paper/Confirmation of Study")]
public class CoS : Paper
{
    public string id;
    public string school;
    public string expiration;
    public bool fakeStamp = false;

    public CoS(PersonToCheck person)
    {
        this.person = person;

        prefab = GameObject.Find("Resources z wishe").GetComponent<PrefabList>().get(PrefabName.CONF_OF_STUDY);

        if(person.firstName.Split(' ').Length > 1)
        {
            name = person.firstName + '\n' + person.lastName;
        }
        else
        {
            name = person.firstName + " " + person.lastName;
        }
        school = person.school;

        // randomly generate expiration date
        
        int year; 
        int month;
        int day;

        year = UnityEngine.Random.Range(2025, 2028 + 1);
        id = person.id;

        if(person.illegal && !person.hasFake)
        {
            float random = UnityEngine.Random.Range(0, 1f);
            if(random < 0.25f)
            {
                year = UnityEngine.Random.Range(2020, 2023 + 1);
                person.paperLegality.has_fake_date = true;
            }
            else if(random < 0.50f)
            {
                do
                {
                    id = "";
                    for(int i = 0; i < 8; ++i)
                    {
                        if(UnityEngine.Random.Range(0, 2) == 1)
                        {
                            id += Convert.ToChar(UnityEngine.Random.Range('A', 'Z'));
                        }
                        else
                        {
                            id += Convert.ToString(UnityEngine.Random.Range(0, 9 + 1));
                        }
                    }
                }
                while(id == person.id);
                person.paperLegality.has_fake_id = true;
            }
            else if(random < 0.75f)
            {
                fakeStamp = true;
                person.paperLegality.has_fake_stamp = true;
            }
            else if(random < 1f)
            {
                name = person.fake_firstName + "\n" + person.fake_lastName;
            
                person.paperLegality.has_fake_name = true;
            }
            person.hasFake = true;
        }

        month = UnityEngine.Random.Range(1, 12 + 1);
        day = UnityEngine.Random.Range(1, DateTime.DaysInMonth(year, month) + 1);

        expiration = day + ". " + month + ". " + year;
    }

    public override GameObject Create()
    {
        game_object = base.Create();    

        TextMeshProUGUI[] text_fields = game_object.GetComponentsInChildren<TextMeshProUGUI>();

        foreach(var text in text_fields)
        {
            switch(text.gameObject.name)
            {
            case("Name"):
                text.text = name;
                break;
            case("Expiration"):
                text.text = expiration;
                break;
            case("ID Number"):
                text.text = Convert.ToString(id);
                break;
            case("School"):
                text.text = school;
                break;
            default:
                throw new Exception("Unknown text field: " + text.gameObject.name);
            }
        }

        GameObject stamps_types = game_object.transform.GetChild(2).GetChild(0).gameObject;
        GameObject stamps = stamps_types.transform.GetChild(Convert.ToInt32(fakeStamp)).gameObject;

        int random_int = UnityEngine.Random.Range(0, stamps.transform.childCount);

        // i dont like this, but whatever
        if(person.gameObject.name != "VSE")
        {
            stamps.transform.GetChild(random_int).gameObject.SetActive(true);
        }

        game_object.GetComponent<PaperSpriteChanger>().switchToSmallSprite();

        return game_object;
    }
}
