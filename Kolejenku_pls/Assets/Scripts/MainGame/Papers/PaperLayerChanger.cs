using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;

/**
 *  <summary>
 *  Handles the overlaying of paper in scene. Last dragged paper is on top of the others.
 *  Is on all the paper prefabs. Every paper for itself.
 *  </summary>
 */
public class PaperLayerChanger : MonoBehaviour
{
    public int base_order = 3;
    public SortingGroup sorting_group;
    public Canvas canvas;
    public Draggable draggable;

    // Start is called before the first frame update
    void Start()
    {
        sorting_group = GetComponent<SortingGroup>();
        draggable = GetComponent<Draggable>();

        GameObject[] papers = GameObject.FindGameObjectsWithTag("Paper");

        if(papers.Length == 1)
        {
            return;
        }

        int max = int.MinValue;
        foreach(var paper in papers)
        {
            if(paper == this)
            {
                continue;
            }

            int order = paper.GetComponent<SortingGroup>().sortingOrder;

            if(order > max)
            {
                max = order;
            }
        }

        sorting_group.sortingOrder = max + 1;
        if(canvas)
        {
            canvas.sortingOrder = max + 1;
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        if(!draggable.isDragging())
        {
            return;
        }

        List<GameObject> papers = new List<GameObject>(GameObject.FindGameObjectsWithTag("Paper"));

        // single papers doesn't need to change its layer
        if(papers.Count == 1)
        {
            return;
        }

        // get sorted papers based on their layer
        papers.Sort
        (
            (lhs, rhs) =>
                lhs.GetComponent<PaperLayerChanger>().sorting_group.sortingOrder.CompareTo(rhs.GetComponent<PaperLayerChanger>().sorting_group.sortingOrder)
        );

        int max = papers[^1].GetComponent<PaperLayerChanger>().sorting_group.sortingOrder;
        
        // paper already on top doesn't need to change
        if(max == sorting_group.sortingOrder)
        {
            return;
        }
        int min = papers[0].GetComponent<PaperLayerChanger>().sorting_group.sortingOrder;


        // this makes the layers not grow to infinity by recalculating the assigned layers
        int maxExpectedOrder = base_order + papers.Count;
        if(max >= maxExpectedOrder)
        {
            int i = 0;
            foreach(var paper in papers)
            {
                paper.GetComponent<PaperLayerChanger>().sorting_group.sortingOrder = base_order + i;
                if(paper.GetComponent<PaperLayerChanger>().canvas)
                {
                    paper.GetComponent<PaperLayerChanger>().canvas.sortingOrder = base_order + i;
                }
                ++i;
            }
        }
        max = papers[^1].GetComponent<PaperLayerChanger>().sorting_group.sortingOrder;

        sorting_group.sortingOrder = max + 1;
        if(canvas)
        {
            canvas.sortingOrder = max + 1;
        }
    }
}
