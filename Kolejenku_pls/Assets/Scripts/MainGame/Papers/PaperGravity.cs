using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

using static PaperSpriteChanger.SpriteType;

// TODO: make it work even for big view

public class PaperGravity : MonoBehaviour
{
    public PaperSpriteChanger paperSpriteChanger;
    public BoxCollider2D paper_area;
    public BoxCollider window_desk_area;
    public Draggable draggable;
    public int falling_speed = 10;

    // Start is called before the first frame update
    void Start()
    {
        paperSpriteChanger = GetComponent<PaperSpriteChanger>();
        paper_area = paperSpriteChanger.paper_area;
        window_desk_area = GameObject.Find("Window Desk Area").GetComponent<BoxCollider>();
        draggable = GetComponent<Draggable>();
    }

    // Update is called once per frame
    void Update()
    {
        if(paperSpriteChanger.spriteType == FULL || paper_area.bounds.Intersects(window_desk_area.bounds) || draggable.isDragging())
        {
            return;
        }

        transform.position += Vector3.down * Time.deltaTime * falling_speed;
    }
}
