using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AccommAgreement : Paper
{    
    public AccommAgreement()
    {
        prefab = GameObject.Find("Resources z wishe").GetComponent<PrefabList>().get(PrefabName.ACCOMM_AGRMT);
        spawn = GameObject.Find("Agreement Spawn Point").transform;
    }
}
