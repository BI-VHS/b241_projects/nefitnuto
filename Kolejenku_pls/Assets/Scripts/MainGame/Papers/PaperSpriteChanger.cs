using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

/**
 *  <summary>
 *  Handles changing the sprite from small to big, depending on the area it's currently in.
 *  </summary>
 */
public class PaperSpriteChanger : MonoBehaviour
{
    public enum SpriteType
    {
        FULL,
        SMALL
    }
    
    public SpriteType spriteType;
    [FormerlySerializedAs("windowArea")] public BoxCollider window_area;
    [FormerlySerializedAs("deskArea")] public BoxCollider desk_area;
    [FormerlySerializedAs("paperArea")] public BoxCollider2D paper_area;
    
    
    public Sprite full_sprite;
    public Sprite small_sprite;

    public GameObject paper;
    public GameObject sprites;

    public GameObject canvas = null;
    
    // Start is called before the first frame update
    void Start()
    {
        GameObject window = GameObject.Find("Window Area");
        window_area = window.GetComponent<BoxCollider>();
        GameObject desk = GameObject.Find("Desk Area");
        desk_area = desk.GetComponent<BoxCollider>();

        transform.position = new Vector3(transform.position.x, transform.position.y, desk.transform.position.z);

        paper_area = gameObject.GetComponent<BoxCollider2D>();
        
        spriteType = SpriteType.FULL;
    }

    // Update is called once per frame
    void Update()
    {
        if (!desk_area.bounds.Intersects(paper_area.bounds) && !window_area.bounds.Intersects(paper_area.bounds))
        {
            return;
        }
        
        if(spriteType == SpriteType.FULL && !desk_area.bounds.Intersects(paper_area.bounds))
        {
            switchToSmallSprite();
        }
        else if (spriteType == SpriteType.SMALL && !window_area.bounds.Intersects(paper_area.bounds))
        {
            switchToFullSprite();
        }
    }

    public void switchToSmallSprite()
    {
        if(canvas)
        {
            canvas.SetActive(false);
        }
        paper.GetComponent<SpriteRenderer>().sprite = small_sprite;
        paper_area.size = paper.GetComponent<SpriteRenderer>().sprite.bounds.size;
        // Debug.Log("full -> small");
        spriteType = SpriteType.SMALL;
        // Vector3 mouse_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        // transform.position = new Vector3(mouse_pos.x, mouse_pos.y, transform.position.z);
        if(sprites)
        {
            sprites.SetActive(false);
        }
    }

    public void switchToFullSprite()
    {
        if(canvas)
        {
            canvas.SetActive(true);
        }
        paper.GetComponent<SpriteRenderer>().sprite = full_sprite;
        paper_area.size = paper.GetComponent<SpriteRenderer>().sprite.bounds.size;
        // Debug.Log("small -> full");
        spriteType = SpriteType.FULL;
        // Vector3 mouse_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        // transform.position = new Vector3(mouse_pos.x, mouse_pos.y, transform.position.z);
        if(sprites)
        {
            sprites.SetActive(true);
        }
    }
}
