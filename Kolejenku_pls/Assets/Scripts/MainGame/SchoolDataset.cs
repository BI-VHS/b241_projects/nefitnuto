using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SchoolDataset
{
    public University[] universities;

    [Serializable]
    public class University
    {
        public string name;
        public string[] faculties;
    }
}
