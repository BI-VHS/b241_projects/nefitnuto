using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Unity.Profiling;
using UnityEngine;

public class Days : MonoBehaviour
{
    public DayList days;
    public TextAsset days_json;

    void Start()
    {
        days = JsonUtility.FromJson<DayList>(days_json.text);
    }

    [Serializable]
    public class DayList
    {
        public Day[] days;
    }

    [Serializable]
    public class Day
    {
        public Rules rules;
        public string[] wanted_people;
        public string[] news;
    }

    [Serializable]
    public class Rules
    {
        public string[] papers_unlocked;
        public string[] person_query;
        public string[] allow_query;
        public string[] floor_denied;
        public string[] nationality_denied;
        public string[] unique_characters;
        public string[] contraband;
        public string[] blok_allowed;
        public string[] special_case;
    }

    //---------------------------------

    public string getRules(int day_number)
    {
        // todo make lookup for rule codes

        string ret = "";

        Day day = days.days[day_number];

        if(day.rules.blok_allowed.Length > 0)
        ret += "Vcházejí osoby mohou být pouze z bloků ";
        foreach(var rule in day.rules.blok_allowed)
        {
            ret += rule.ToString().Substring(1) + ", ";
        }
        if(day.rules.blok_allowed.Length > 0)
        ret += "\n\n";
        if(day.rules.floor_denied.Length > 0)
        ret += "Nepouštět rezidenty bloku 13, pokud jsou z pater ";
        foreach(var rule in day.rules.floor_denied)
        {
            ret += rule.ToString().Substring(1) + ", ";
        }
        if(day.rules.floor_denied.Length > 0)
        ret += "\n\n";
        if(day.rules.allow_query.Length > 0)
        ret += "Povolené žádosti jsou pouze ";
        foreach(var rule in day.rules.allow_query)
        {
            ret += rule.ToString().ToLower() + ", ";
        }
        if(day.rules.allow_query.Length > 0)
        ret += "\n\n";
        if(day.rules.nationality_denied.Length > 0)
        ret += "Nepouštět nikoho s národností ";
        foreach(var rule in day.rules.nationality_denied)
        {
            ret += rule.ToString().ToLower() + ", ";
        }
        if(day.rules.nationality_denied.Length > 0)
        ret += "\n\n";
        // if(day.rules.nationality_denied.Length > 0)
        // ret += "Povolovat papíry ";
        // foreach(var rule in day.rules.papers_unlocked)
        // {
        //     ret += rule.ToString().ToLower() + ", ";
        // }
        // ret += "\n";
        foreach(var rule in day.rules.special_case)
        {
            ret += "Kontrolovat identitu společně s kolejenkou.";
            // ret += rule.ToString().ToLower() + ", ";
        }
        ret += "\n";

        return ret;
    }

    //---------------------------------

    public bool isWanted(int day_number, string name)
    {
        Day day = days.days[day_number];
        foreach(var wanted in day.wanted_people)
        {
            if(name == wanted)
            {
                return true;
            }
        }
        return false;
    }

    //--------------------------------

    public bool isRuleActive(int day_number, RulesEnum.Blok blok_rule)
    {
        Day day = days.days[day_number];
        foreach(var blok in day.rules.blok_allowed)
        {
            Debug.Log(blok.ToString() + " " + blok_rule.ToString());
            if(blok == blok_rule.ToString())
            {
                return true;
            }
        }
        return false;
    }

    public bool isRuleActive(int day_number, RulesEnum.Floor floor_rule)
    {
        Day day = days.days[day_number];
        foreach(var floor in day.rules.floor_denied)
        {
            if(floor == floor_rule.ToString())
            {
                return true;
            }
        }
        return false;
    }

    public bool isRuleActive(int day_number, RulesEnum.Paper paper_rule)
    {
        Day day = days.days[day_number];
        foreach(var paper in day.rules.papers_unlocked)
        {
            if(paper == paper_rule.ToString())
            {
                return true;
            }
        }
        return false;
    }

    public bool isRuleActive(int day_number, RulesEnum.Nationality nationality_rule)
    {
        Day day = days.days[day_number];
        foreach(var nationality in day.rules.nationality_denied)
        {
            if(nationality == nationality_rule.ToString())
            {
                return true;
            }
        }
        return false;
    }

    public bool isRuleActive(int day_number, RulesEnum.Contraband contraband_rule)
    {
        Day day = days.days[day_number];
        foreach(var contraband in day.rules.contraband)
        {
            if(contraband == contraband_rule.ToString())
            {
                return true;
            }
        }
        return false;
    }

    public bool isRuleActive(int day_number, RulesEnum.PersonQuery person_query_rule)
    {
        Day day = days.days[day_number];
        foreach(var person_query in day.rules.person_query)
        {
            if(person_query == person_query_rule.ToString())
            {
                return true;
            }
        }
        return false;
    }

    public bool isRuleActive(int day_number, RulesEnum.AllowQuery allow_query_rule)
    {
        Day day = days.days[day_number];
        foreach(var allow_query in day.rules.allow_query)
        {
            if(allow_query == allow_query_rule.ToString())
            {
                return true;
            }
        }
        return false;
    }

    public bool isRuleActive(int day_number, RulesEnum.SpecialCases special_case_rule)
    {
        Day day = days.days[day_number];
        foreach(var special in day.rules.special_case)
        {
            if(special == special_case_rule.ToString())
            {
                return true;
            }
        }
        return false;
    }

    public bool isRuleActive(int day_number, RulesEnum.UniqueCharacters unique_characters_rule)
    {
        Day day = days.days[day_number];
        foreach(var unique_characters in day.rules.unique_characters)
        {
            if(unique_characters == unique_characters_rule.ToString())
            {
                return true;
            }
        }
        return false;
    }
}
