using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[Serializable]
public class PersonDataset
{
    public Country cz;
    public Country sk;
    public Country ind;
    public Country pl;
    public Country ge;
    public Country tr;
    public Country ru;
    public Country ua;
    public Country jp;

    [Serializable]
    public class Country
    {
        public string country;
        public string nationality;
        public ColorWeights skin_color_probability;
        public Names names;
    }

    [Serializable]
    public class ColorWeights
    {
        public int white;
        public int black;
        public int middle_east;
        public int asian;
    }

    [Serializable]
    public class Names
    {
        public Gender male;
        public Gender female;        
    }

    [Serializable]
    public class Gender
    {
        public string[] first;
        public string[] last;
    }

    public PersonDataset()
    {
    }
}
