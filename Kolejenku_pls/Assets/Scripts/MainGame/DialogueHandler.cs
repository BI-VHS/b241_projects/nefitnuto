using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogueHandler : MonoBehaviour
{
    public enum DialogueChoice
    {
        REGISTER_ENTRY,
        OPEN_ENTRY,
        ALLOW,
        DENY,
        WRONG,
        WRONG_BLOK,
        WRONG_DATE,
        WRONG_ID,
        WRONG_NAME,
        WRONG_PHOTO,
        WRONG_STAMP
    }

    public GameObject dialogue_bubble;
    public GameObject[] dialogue_sides;

    public Queue<Tuple<GameObject, AudioSource>> dialogue_queue;
    private List<GameObject> played_dialogue;
    public TextAsset player_json;
    public AudioSource player_voice;
    public AudioSource generic_voice; // i cant be bothered to have it somewhere else
    public GameObject current_message;
    public int delay = 1;
    public DialogueDataset player_dialogue;
    private bool is_dialogue_playing = false;

    void Start()
    {
        dialogue_queue = new Queue<Tuple<GameObject, AudioSource>>();
        player_dialogue = JsonUtility.FromJson<DialogueDataset>(player_json.text);
    }

    public IEnumerator play()
    {
        if(is_dialogue_playing)
        {
            yield return new WaitUntil(() => !is_dialogue_playing); // Exit if the coroutine is already running
            if(dialogue_queue.Count == 0)
            {
                yield break;
            }
        }

        is_dialogue_playing = true;

        // attempt for quicker response
        yield return new WaitForEndOfFrame();


        while(dialogue_queue.Count > 0)
        {
            Tuple<GameObject, AudioSource> queue_item = dialogue_queue.Dequeue();
            current_message = queue_item.Item1;
            current_message.SetActive(true);
            if(current_message.transform.IsChildOf(dialogue_sides[0].transform))
            {
                current_message.GetComponent<Animator>().SetTrigger("person");
            }
            else
            {
                current_message.GetComponent<Animator>().SetTrigger("player");
            }
            // played_dialogue.Add(current_message);
            queue_item.Item2.pitch = UnityEngine.Random.Range(0.95f, 1.11f);
            int d = delay / 5;
            yield return new WaitForSeconds(d / 2);
            queue_item.Item2.Play();
            yield return new WaitForSeconds(d / 2);
        }
        yield return new WaitForSeconds(delay / 5);
        is_dialogue_playing = false;

        // clean up
        // foreach(var bubble in played_dialogue)
        // {
        //     Destroy(bubble);
        // }
    }

    public void addDialogue(DialogueChoice choice)
    {
        string[] generic_choice = getGenericChoice(PersonToCheck.dialogue, choice);
        string[] player_choice = getPlayerChoice(player_dialogue, choice);

        int random_generic = UnityEngine.Random.Range(0, generic_choice.Length);
        int random_player = UnityEngine.Random.Range(0, player_choice.Length);

        GameObject new_bubble_generic = GameObject.Instantiate(dialogue_bubble, dialogue_sides[0].transform);
        GameObject new_bubble_player = GameObject.Instantiate(dialogue_bubble, dialogue_sides[1].transform);
        
        new_bubble_generic.GetComponentInChildren<TextMeshProUGUI>().text = generic_choice[random_generic];
        new_bubble_player.GetComponentInChildren<TextMeshProUGUI>().text = player_choice[random_player];
        
        dialogue_queue.Enqueue(new Tuple<GameObject, AudioSource>(new_bubble_player, player_voice));
        dialogue_queue.Enqueue(new Tuple<GameObject, AudioSource>(new_bubble_generic, generic_voice));
    }

    public void addUniqueDialogue(DialogueChoice choice, PersonToCheck person)
    {
        UniqueDialogueDataset dialogue = person.unique_dialogue;

        var unique_choice = getUniqueChoice(dialogue, choice);
        
        for(int i = 0; i < unique_choice.Length; i++)
        {
            int side = Convert.ToInt16(unique_choice[i].is_player);
            GameObject new_bubble = GameObject.Instantiate(dialogue_bubble, dialogue_sides[side].transform);
            new_bubble.GetComponentInChildren<TextMeshProUGUI>().text = unique_choice[i].text;
            AudioSource voice = unique_choice[i].is_player ? player_voice : person.unique_voice;
            
            dialogue_queue.Enqueue(new Tuple<GameObject, AudioSource>(new_bubble, voice));
        }
    }

    private string[] getGenericChoice(DialogueDataset dialogue, DialogueChoice choice) => choice switch
    {
        DialogueChoice.REGISTER_ENTRY => dialogue.register_entry,
        DialogueChoice.OPEN_ENTRY => dialogue.open_entry,
        DialogueChoice.ALLOW => dialogue.allow,
        DialogueChoice.DENY => dialogue.deny,
        DialogueChoice.WRONG => dialogue.wrong,
        DialogueChoice.WRONG_BLOK => dialogue.wrong_blok,
        DialogueChoice.WRONG_DATE => dialogue.wrong_date,
        DialogueChoice.WRONG_ID => dialogue.wrong_id,
        DialogueChoice.WRONG_NAME => dialogue.wrong_name,
        DialogueChoice.WRONG_PHOTO => dialogue.wrong_photo,
        DialogueChoice.WRONG_STAMP => dialogue.wrong_stamp,
        _ => new string[] {}
    };

    private string[] getPlayerChoice(DialogueDataset dialogue, DialogueChoice choice) => choice switch
    {
        DialogueChoice.REGISTER_ENTRY => dialogue.register_entry,
        DialogueChoice.OPEN_ENTRY => dialogue.open_entry,
        DialogueChoice.ALLOW => dialogue.allow,
        DialogueChoice.DENY => dialogue.deny,
        DialogueChoice.WRONG => dialogue.wrong,
        DialogueChoice.WRONG_BLOK => dialogue.wrong_blok,
        DialogueChoice.WRONG_DATE => dialogue.wrong_date,
        DialogueChoice.WRONG_ID => dialogue.wrong_id,
        DialogueChoice.WRONG_NAME => dialogue.wrong_name,
        DialogueChoice.WRONG_PHOTO => dialogue.wrong_photo,
        DialogueChoice.WRONG_STAMP => dialogue.wrong_stamp,
        _ => new string[] {}
    };

    private UniqueDialogueDataset.Dialogue[] getUniqueChoice(UniqueDialogueDataset dialogue, DialogueChoice choice) => choice switch
    {
        DialogueChoice.REGISTER_ENTRY => dialogue.register_entry,
        DialogueChoice.OPEN_ENTRY => dialogue.open_entry,
        DialogueChoice.ALLOW => dialogue.allow,
        DialogueChoice.DENY => dialogue.deny,
        DialogueChoice.WRONG => dialogue.wrong,
        _ => new UniqueDialogueDataset.Dialogue[] {}
    };
}
