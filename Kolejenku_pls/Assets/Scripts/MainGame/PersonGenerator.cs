using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;


public class PersonGenerator
{
    public PersonDataset person_dataset;
    public SchoolDataset school_dataset;
    public PrefabList prefabs;
    public GameObject parent;

    public PersonGenerator(string person_json, string school_json)
    {
        person_dataset = JsonUtility.FromJson<PersonDataset>(person_json);
        school_dataset = JsonUtility.FromJson<SchoolDataset>(school_json);

        prefabs = GameObject.Find("Resources z wishe").GetComponent<PrefabList>();
        parent = GameObject.Find("Characters");
    }

    public GameObject Create()
    {
        GameObject ret;
        bool gender = Convert.ToBoolean(UnityEngine.Random.Range(0, 2));

        if(gender)
        {
            ret = GameObject.Instantiate(prefabs.get(PrefabName.CHAR_M));
        }
        else
        {
            ret = GameObject.Instantiate(prefabs.get(PrefabName.CHAR_F));
        }
        // ret.SetActive(false);
        ret.GetComponent<SortingGroup>().sortingOrder = 3;
        ret.transform.SetParent(parent.transform);

        PersonToCheck person = ret.GetComponent<PersonToCheck>();
        PersonDataset.Country country_code = getCountryCode();

        person.gender = gender;

        setPersonName(person, country_code);

        person.country = country_code.country;
        person.nationality = country_code.nationality;
    
        setSkinColor(ret, country_code.skin_color_probability);
        
        person.registered = true;
        float f = UnityEngine.Random.Range(0, 1f);
        if(f > 0.80f)
        {
            person.blockNumber = UnityEngine.Random.Range(1, 13);
        }
        else
        {
            person.blockNumber = 13;
        }
        person.roomNumber = UnityEngine.Random.Range(1, 699);
        person.school = getSchool();

        person.id = "";
        for(int i = 0; i < 8; ++i)
        {
            if(UnityEngine.Random.Range(0, 2) == 1)
            {
                person.id += Convert.ToChar(UnityEngine.Random.Range('A', 'Z'));
            }
            else
            {
                person.id += Convert.ToString(UnityEngine.Random.Range(0, 9 + 1));
            }
            if(i == 3)
            {
                person.id += '-';
            }
        }

        return ret;
    }

    private PersonDataset.Country getCountryCode() =>
        UnityEngine.Random.Range(0, 9) switch
        {
            0 => person_dataset.cz,
            1 => person_dataset.sk,
            2 => person_dataset.ind,
            3 => person_dataset.pl,
            4 => person_dataset.jp,
            5 => person_dataset.ge,
            6 => person_dataset.ru,
            7 => person_dataset.ua,
            8 => person_dataset.tr,
            _ => person_dataset.cz // Default case to handle unexpected values
        };

    private void setSkinColor(GameObject game_object, PersonDataset.ColorWeights skin_weights)
    {
        GameObject face = game_object.transform.GetChild(1).gameObject;
        GameObject[] skin_colors = new GameObject[face.transform.childCount];
        for(int i = 0; i < face.transform.childCount; ++i)
        {
            skin_colors[i] = face.transform.GetChild(i).gameObject;
        }

        int total_weight = skin_weights.white + skin_weights.middle_east + skin_weights.asian + skin_weights.black;

        int random = UnityEngine.Random.Range(0, total_weight);
        if(random < skin_weights.white)
        {
            skin_colors[0].SetActive(true);
            Debug.Log("white");
        }
        else if(random < skin_weights.white + skin_weights.asian && random > skin_weights.white)
        {
            skin_colors[1].SetActive(true);
            Debug.Log("asian");
        }
        else if(random < skin_weights.white + skin_weights.asian + skin_weights.black && random > skin_weights.white + skin_weights.asian)
        {
            skin_colors[2].SetActive(true);
            Debug.Log("black");
        }
        else
        {
            skin_colors[3].SetActive(true);
            Debug.Log("middle east");
        }
    }

    private void setPersonName(PersonToCheck person, PersonDataset.Country country_code)
    {
        if(person.gender)
        {
            person.firstName = country_code.names.male.first[UnityEngine.Random.Range(0, country_code.names.male.first.Length)];
            person.lastName = country_code.names.male.last[UnityEngine.Random.Range(0, country_code.names.male.last.Length)];
        
            do
            {
                person.fake_firstName = country_code.names.male.first[UnityEngine.Random.Range(0, country_code.names.male.first.Length)];
                person.fake_lastName = country_code.names.male.last[UnityEngine.Random.Range(0, country_code.names.male.last.Length)];
            } while(person.firstName == person.fake_firstName || person.lastName == person.fake_lastName);
        }
        else
        {
            person.firstName = country_code.names.female.first[UnityEngine.Random.Range(0, country_code.names.female.first.Length)];
            person.lastName = country_code.names.female.last[UnityEngine.Random.Range(0, country_code.names.female.last.Length)];
                        
            do
            {
                person.fake_firstName = country_code.names.male.first[UnityEngine.Random.Range(0, country_code.names.male.first.Length)];
                person.fake_lastName = country_code.names.male.last[UnityEngine.Random.Range(0, country_code.names.male.last.Length)];
            } while(person.firstName == person.fake_firstName || person.lastName == person.fake_lastName);
        }
    }

    private string getSchool()
    {
        SchoolDataset.University uni;
    
        float random_f = UnityEngine.Random.Range(0, 1f);
        if(random_f < 0.95)
        {
            uni = school_dataset.universities[0];
        }
        else
        {
            uni = school_dataset.universities[1];
        }

        int random_int = UnityEngine.Random.Range(0, uni.faculties.Length);
        
        return uni.name + " " + uni.faculties[random_int];
    }
}
