using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class PapersInvestigation : MonoBehaviour
{
    public GameObject node_prefab;
    
    public PeopleAtDesks people;

    public bool is_active;

    public void SetActive(bool value)
    {
        if(!is_active)
        {
            StartCoroutine(InvestigationMode());
        }
    }

    // Update is called once per frame
    void Update()
    {
        input();
    }

    void input()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            is_active = !is_active;
            SetActive(is_active);
        }
    }

    void SetUpInvestigation()
    {
        GameObject[] checkable = GameObject.FindGameObjectsWithTag("Checkable");

        foreach(var x in checkable)
        {
            GameObject node = GameObject.Instantiate(node_prefab);
            node.transform.position = x.transform.position;
        }
    }

    IEnumerator InvestigationMode()
    {
        SetUpInvestigation();

        while(is_active)
        {
            
        }
        yield return null;
    }
}
