using UnityEngine;

public class PauseMenu : MonoBehaviour {
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private bool isPaused = false;

    void Start() {
        pauseMenu.SetActive(false);
        isPaused = false;
    }

    void Update() {
        // pause game if esc pressed and game is not paused
        if (Input.GetKeyDown(KeyCode.Escape) && !isPaused)
            pauseGame();

        // unpause game if esc is pressed and game is paused
        else if (Input.GetKeyDown(KeyCode.Escape) && isPaused)
            unpauseGame();
    }

    // set time scale to 0 and show pause menu
    public void pauseGame() {
        isPaused = true;
        pauseMenu.SetActive(true);
        Time.timeScale = 0.0f;
    }

    // set time scale to 1 and hide pause menu
    public void unpauseGame() {
        isPaused = false;
        pauseMenu.SetActive(false);
        Time.timeScale = 1.0f;
    }

    public void goToMenu() {
        Time.timeScale = 1.0f;
        SceneSwitcher switcher = FindAnyObjectByType<SceneSwitcher>();
        switcher.LoadMainMenu();
    }
}
