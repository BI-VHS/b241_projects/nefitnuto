using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PaperLegality
{
    public bool has_fake_name = false;
    public bool has_fake_blok_number = false;
    public bool has_fake_id = false;
    public bool has_fake_photo = false;
    public bool has_fake_stamp = false;
    public bool has_fake_date = false;
}
