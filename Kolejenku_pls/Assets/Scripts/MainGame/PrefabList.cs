using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public enum PrefabName
{
    KOLEJENKA,
    ID_CZ,
    ID_SK,
    PASSPORT,
    CONF_OF_STUDY, // confirmation of study
    ACCOMM_AGRMT, // accommodation agreement
    CHAR_M,
    CHAR_F,
    FINE,
    RULEBOOK
}

/**
 *  <summary>
 *  Easier lookup for prefabs instead of using project file system.
 *  Needs to be filled manually in editor.
 *  </summary>
 */
public class PrefabList : MonoBehaviour
{
    [Serialize]
    public GameObject[] prefabs;

    /**
     *  <returns>prefab of `prefab_name`</returns>>
     */
    public GameObject get(PrefabName prefab_name)
    {
        return prefabs[Convert.ToInt32(prefab_name)];
    }
}
