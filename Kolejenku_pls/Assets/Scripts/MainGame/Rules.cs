using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RulesEnum
{
    public enum Paper
    {
        KOLEJENKA,
        ID,
        COS,
        PASSPORT,
        ACCOMMODATION_AGREEMENT,
        INSURANCE
    }

    public enum Blok
    {
        B1,
        B2,
        B3,
        B4,
        B5,
        B6,
        B7,
        B8,
        B9,
        B10,
        B11,
        B12,
        B13
    }

    public enum PersonQuery
    {
        REGISTER,
        OPEN
    }

    public enum AllowQuery
    {
        REGISTER,
        OPEN
    }

    public enum Floor
    {
        F0,
        F1,
        F2,
        F3,
        F4,
        F5,
        F6
    }

    public enum Contraband
    {
        DRUGS,
        FIRE
    }

    public enum Nationality
    {
        CZECH,
        SLOVAK,
        JAPANESE,
        INDIAN,
        POLISH,
        GERMAN,
        TURKISH,
        RUSSIAN,
        UKRAINIAN
    }

    public enum UniqueCharacters
    {
        SUZ,
        SKIBIDI,
        VSE,
        METRO
    }

    public enum SpecialCases
    {
        CROSSCHECK_KOLEJENKA
    }
}
