using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 *  <summary>
 *  Used for lighting up windows in the entrance area's top left corner AKA the outside.
 *  Lights up the windows in random order since specified time. Default is around 4 o'clock on the entrance clock.
 *  </summary>
 */
public class WindowLightsController : MonoBehaviour
{
    public GameObject timer;
    public List<GameObject> window_lights; // is filled manually in editor.
    public int start_seconds = 320;

    public float light_cooldown = 0.325f;
    private float cooldown = 0;

    // Update is called once per frame
    void Update()
    {
        if(timer.GetComponent<Timer>().getTimePassed() < start_seconds)
        {
            return;
        }
        
        // first light up the door light
        if(!window_lights[^1].activeInHierarchy)
        {
            window_lights[^1].SetActive(true);
            cooldown = light_cooldown;
            return;
        }

        if(cooldown > 0)
        {
            cooldown -= Time.deltaTime; // TODO probably need do-over 
            return;
        }

        foreach(var window in window_lights)
        {
            if(window.activeInHierarchy)
            {
                continue;
            }

            System.Random random = new System.Random();

            if(Mathf.FloorToInt(random.Next(0, 2)) < 1)
            {
                window.SetActive(true);
                break;
            }

        }

        cooldown = light_cooldown;
    }
}
