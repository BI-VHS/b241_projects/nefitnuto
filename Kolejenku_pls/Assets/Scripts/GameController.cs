using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using TMPro;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using static STATE;

public class GameController : MonoBehaviour {
    public int winConditionMoney;

    [Space]

    public GameObject bomb_object;
    public GameObject bomb_book_prefab;

    [Space]

    [SerializeField] private QueueHandler outsideQueue_handler;
    [SerializeField] private List<String> MiniGamesSceneList;

    [Space]

    public PeopleAtDesks people;
    bool has_documents = true;
    bool visitor_processed = false;

    public GameState game_state;
    public DayStatistics day_statistics;
    public GameObject day_endscreen;

    [Space]

    public TextMeshProUGUI news;
    public TextMeshProUGUI rules;

    [Space]

    public EntranceState state;
    public Days days;
    public Timer timer;

    [Space]

    // refactor time :D
    public DecisionButtonsManager buttons;
    public QueueManager queue_manager;
    public DialogueHandler dialogue;

    void Start()
    {
        GameObject gameObject = GameObject.Find("GameState");
        game_state = gameObject.GetComponent<GameState>();
        days = gameObject.GetComponent<Days>();

        buttons = new DecisionButtonsManager();
        queue_manager = new QueueManager(game_state, days);

        if(game_state[MONEY] >= winConditionMoney && game_state[DAY] == days.days.days.Length - 1)
        {
            StartCoroutine(GameObject.FindFirstObjectByType<SceneSwitcher>().LoadLevel("VictoryScreen"));
        }
        else if(game_state[MONEY] < winConditionMoney && game_state[DAY] == days.days.days.Length - 1)
        {
            StartCoroutine(GameObject.FindFirstObjectByType<SceneSwitcher>().LoadLevel("GameOverScreen"));
        }
        else
        {
            state = EntranceState.START_DAY;
        }
    }
    
    // TODO: do better

    void Update() {
        input();

        NewLoop();
    }

    void input()
    {
    }

    void NewLoop()
    {
        // handle queue generation
        // |-> according to rules of day
        // |-> push uniques
        // handle arrival
        // |-> give documents
        // |-> play dialogue
        // handle deny/allow
        // |->check legality
        // |  |-> if wrong decision, print a fine
        // |->add money
        // at the end of the day, dont generate new people, when ran out, pick minigame, change to hallway

        switch(state)
        {
        case EntranceState.START_DAY:
            startDay();
            break;
        case EntranceState.GAME:
            game();
            break;
        case EntranceState.END_DAY:
            break;
        }
    }

    private void startDay()
    {
        queue_manager.CreateQueue();
        
        string news_text = "";

        foreach(var str in days.days.days[game_state[DAY]].news)
        {
            news_text += str + "\n\n";
        }

        news.text = news_text;
        rules.text = "Pravidla:" + "\n\n" + days.getRules(game_state[DAY]);

        state = EntranceState.GAME;
    }

    GameObject agg;
    bool entry_dialogue = false;

    public void AllowPerson()
    {
        if
        (
            agg != null && !agg.GetComponent<AgreementFunction>().isSigned()
        && !people.GetCurrentPerson().GetPerson().illegal
        )
        {
            people.GetCurrentPerson().GetPerson().illegal = true;
        }

        // another function
        if(!people.GetCurrentPerson().GetPerson().isUnique)
        {
            dialogue.addDialogue(DialogueHandler.DialogueChoice.ALLOW);
        }
        else
        {
            dialogue.addUniqueDialogue(DialogueHandler.DialogueChoice.ALLOW, people.GetCurrentPerson().GetPerson());
        }

        // another function
        if(people.GetCurrentPerson().GetPerson().nationality == "Indian")
        {
            game_state[INDIANS_ALLOWED]++;
        }

        StartCoroutine(ProcessPerson(true));
    }

    public void DenyPerson()
    {
        var person = people.GetCurrentPerson().GetPerson();

        if
        (
            agg != null && agg.GetComponent<AgreementFunction>().isSigned() 
        && !people.GetCurrentPerson().GetPerson().illegal
        )
        {
            people.GetCurrentPerson().GetPerson().illegal = true;
        }
        
        if(person.paperLegality.has_fake_blok_number)
        {
            dialogue.addDialogue(DialogueHandler.DialogueChoice.WRONG_BLOK);
        }
        else if(person.paperLegality.has_fake_date)
        {
            dialogue.addDialogue(DialogueHandler.DialogueChoice.WRONG_DATE);
        }
        else if(person.paperLegality.has_fake_id)
        {
            dialogue.addDialogue(DialogueHandler.DialogueChoice.WRONG_ID);
        }
        else if(person.paperLegality.has_fake_name)
        {
            dialogue.addDialogue(DialogueHandler.DialogueChoice.WRONG_NAME);
        }
        else if(person.paperLegality.has_fake_photo)
        {
            dialogue.addDialogue(DialogueHandler.DialogueChoice.WRONG_PHOTO);
        }
        else if(person.paperLegality.has_fake_stamp)
        {
            dialogue.addDialogue(DialogueHandler.DialogueChoice.WRONG_STAMP);
        }

        // another function
        if(!people.GetCurrentPerson().GetPerson().isUnique)
        {
            dialogue.addDialogue(DialogueHandler.DialogueChoice.DENY);
        }
        else
        {
            dialogue.addUniqueDialogue(DialogueHandler.DialogueChoice.DENY, people.GetCurrentPerson().GetPerson());
        }

        StartCoroutine(ProcessPerson(false));
    }

    private IEnumerator ProcessPerson(bool allowed)
    {
        day_statistics.person_count++;

        day_statistics.money_made += 50;
        game_state[MONEY] += 50;

        var current_person = people.GetCurrentPerson().GetPerson();

        if(current_person.isUnique)
        {
            UpdateUniqueStats(current_person, allowed);
        }

        current_person.DestroyDocuments();

        yield return new WaitForEndOfFrame();

        // check if the aggreement was destroyed or not, if not then destroy
        if (agg != null)
        {
            Destroy(agg);
        }
        
        yield return StartCoroutine(dialogue.play());
        yield return new WaitForSeconds(0.5f);

        if(allowed && current_person.gameObject.name == "BombExpert(Clone)")
        {
            var spawn = GameObject.Find("Papers Spawn Point").transform;
            GameObject.Instantiate(bomb_book_prefab, spawn);
        }

        if
        (
            current_person.gameObject.name == "VSE(Clone)" && game_state[VSE_APPEAR] == 2
        )
        {
            if(game_state[VSE_DENY] == 2)
            {
                StartCoroutine(bomb());
            }
            else
            {
                // we dont need the expert if theres no bomb
                var expert = people.personsAtQueue.Dequeue();
                expert.gameObject.SetActive(false);
            }
        }

        people.MakeDecision(allowed);

        if
        (
            (
            allowed && current_person.illegal
        ||  !allowed && !current_person.illegal)
        && !current_person.is_visitor
        )
        {
            int fine_count = ++game_state[FINES_RECEIVED];
            day_statistics.fines_count++;
            string penalization;
            if(fine_count < 2)
            {
                penalization = "Bez penalizace.";
            }
            else
            {
                penalization = "Ztrhnuto 50 Kč z výplaty.";
                game_state[MONEY] -= 50;
                day_statistics.fines_price += 50;
            }

            string purpose;

            if(allowed)
            {
                purpose = "Vpuštění osoby bez správných údajů.";
            }
            else
            {
                purpose = "Odmítnutí osoby se správnými údaji.";
            }
            Fine fine = new Fine(purpose + " " + penalization);
            fine.Create();
        }

        if(people.personsAtQueue.Count == 0) {
            if(timer.secondsLeft == 0) {
                game_state[DAY]++;
                state = EntranceState.END_DAY;
                StartCoroutine(endDay());
                yield break;
            }
            queue_manager.AddPeople(5);
        }
    }

    private void UpdateUniqueStats(PersonToCheck current_person, bool allowed)
    {
        switch(current_person.gameObject.name)
        {
        case "Skibidi(Clone)":
            if(allowed) game_state[SKIBIDI_ALLOW]++;
            else game_state[SKIBIDI_DENY]++;
            break;
        case "Metro(Clone)":
            if(allowed) game_state[METRO_ALLOW]++;
            else game_state[METRO_DENY]++;
            break;
        case "VSE(Clone)":
            if(allowed) game_state[VSE_ALLOW]++;
            else game_state[VSE_DENY]++;
            break;
        case "Suz(Clone)":
            break;
        default:
            break;
        }
    }

    // i would like this to be a coroutine, but i wouldnt know where to call it from
    // maybe some event, idk
    private void game()
    {
        if(people.GetCurrentPerson() == null)
        {
            if(!has_documents)
            {
                has_documents = true;
            }

            visitor_processed = false;
            
            return;
        }

        PersonToCheck current_person = people.GetCurrentPerson().GetPerson();

        bool is_person_entering = current_person.transform.position.x < people.personToCheckWaypoints[1].transform.position.x;
        if(is_person_entering)
        {
            entry_dialogue = false;
            return;
        }

        if(!entry_dialogue)
        {
            if(current_person.isUnique)
            {
                if(current_person.registered)
                {
                    dialogue.addUniqueDialogue(DialogueHandler.DialogueChoice.OPEN_ENTRY, current_person);
                }
                else
                {
                    dialogue.addUniqueDialogue(DialogueHandler.DialogueChoice.REGISTER_ENTRY, current_person);
                }
                print("here");
            }
            else if(current_person.registered)
            {
                dialogue.addDialogue(DialogueHandler.DialogueChoice.OPEN_ENTRY);
            }
            else
            {
                dialogue.addDialogue(DialogueHandler.DialogueChoice.REGISTER_ENTRY);
            }
            entry_dialogue = true;
            StartCoroutine(dialogue.play());
        }

        if(current_person.is_visitor)
        {
            if(!visitor_processed)
            {
                DenyPerson();
                visitor_processed = true;
            }

            return;
        }

        if(has_documents)
        {
            current_person.SpawnDocuments();

            if(!current_person.registered)
            {
                AccommAgreement agreement = new AccommAgreement();
                agg = agreement.Create();
                agg.GetComponent<PaperSpriteChanger>().switchToSmallSprite();
                // current_person.GetGameObjectDocuments().Add(agg);
            }

            has_documents = false;

            return;
        }

        if(agg != null && !current_person.IsKolejenkaPrinted() && agg.GetComponent<AgreementFunction>().isSigned())
        {
            current_person.PrintKolejenka();
        }

        if(current_person.GetGameObjectDocuments().Count == 0 && current_person.gameObject.name != "BombExpert(Clone)")
        {
            buttons.SetActive(false);

            return;
        }

        foreach(var paper in current_person.GetGameObjectDocuments())
        {
            PaperSpriteChanger changer = paper.GetComponent<PaperSpriteChanger>();
            bool are_all_papers_in_return_area =
                current_person.gameObject.name != "Suz(Clone)"
                && (changer.spriteType == PaperSpriteChanger.SpriteType.FULL
                || !changer.paper_area.bounds.Intersects(GameObject.Find("Paper Return Area").GetComponent<BoxCollider>().bounds));
            if(are_all_papers_in_return_area)
            {
                buttons.SetActive(false);

                return;
            }
        }

        buttons.SetActive(true);
    }

    private IEnumerator endDay() {
        // todo play audio

        outsideQueue_handler.MakeEveryoneLeave();

        yield return new WaitForEndOfFrame();

        // wait until everyone from the outside queue leaves the scene
        while(outsideQueue_handler.transform.childCount != 0) {
            yield return null;
        }

        // little delay
        yield return new WaitForSeconds(2f);

        day_endscreen.SetActive(true);
        day_endscreen.GetComponent<Animator>().SetTrigger("EndDayGame");

        while(day_endscreen.activeInHierarchy) {
            yield return null;
        }

        int random = UnityEngine.Random.Range(0, MiniGamesSceneList.Count);
        StartCoroutine(GameObject.FindFirstObjectByType<SceneSwitcher>().LoadLevel(MiniGamesSceneList[random]));

    }

    private IEnumerator bomb()
    {
        bomb_object.SetActive(true);

        var bomb_diff = bomb_object.GetComponent<BombDiffusion>();

        yield return new WaitUntil(() => bomb_diff.isDiffused());

        yield return new WaitForSeconds(1f);

        bomb_object.SetActive(false);
    }
}

public enum EntranceState {
    START_DAY,
    GAME,
    END_DAY
}