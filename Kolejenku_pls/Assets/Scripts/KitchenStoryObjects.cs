using UnityEngine;

public class KitchenStoryObjects : MonoBehaviour {
    [SerializeField] private GameObject[] states;
    [SerializeField] private GameObject skibidi;
    [SerializeField] private GameState gameState;
    private int stateID = 0;
    private bool isWashed = false;

    void Start() {
        // turn off all story objects
        skibidi.SetActive(false);
        foreach (GameObject obj in states)
            obj.SetActive(false);


        // find game state object, or return
        GameObject stateObj = GameObject.FindGameObjectWithTag("GameState");
        if (stateObj == null) return;
        gameState = stateObj.GetComponent<GameState>();


        // if skibidi, then toilet
        if (gameState[STATE.SKIBIDI_ALLOW] != 0)
            skibidi.SetActive(true);


        int indianCount = gameState[STATE.INDIANS_ALLOWED];
        int dishesWashedCount = gameState[STATE.DISHES_WASHED_COUNT];

        // check if dishes have been washed before a new set of indians (12) has been accepted into dorm
        // if no display dishes, if yes don't
        if (dishesWashedCount >= indianCount / 12)
            return;

        // depending on state, turn on states
        if (indianCount % 12 == 0 && indianCount != 0) {
            stateID = 3;
            states[stateID].SetActive(true);
        }
        else if (indianCount % 12 > 8) {
            stateID = 2;
            states[stateID].SetActive(true);
        }
        else if (indianCount % 12 > 5) {
            stateID = 1;
            states[stateID].SetActive(true);
        }
        else if (indianCount % 12 > 2) {
            stateID = 0;
            states[stateID].SetActive(true);
        }
    }

    void Update() {
        // increase count of dishes washed
        if (!isWashed && states[stateID].transform.childCount == 0) {
            ++gameState[STATE.DISHES_WASHED_COUNT];
            isWashed = true;
        }

        // temp solution for previewing, to be deleted
        #region delete
        if (Input.GetKeyDown(KeyCode.Space)) {
            foreach (GameObject obj in states) {
                obj.SetActive(false);
            }

            if (stateID <= states.Length - 1) {
                states[stateID].SetActive(true);
            }

            stateID = ++stateID % (states.Length + 1);
        }

        if (Input.GetKeyDown(KeyCode.S)) {
            skibidi.SetActive(!skibidi.activeInHierarchy);
        }
        #endregion
    }
}
