using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnClick : MonoBehaviour
{
    public AudioSource sound;

    void OnMouseDown()
    {
        if(!sound.isPlaying)
        {
            sound.pitch = UnityEngine.Random.Range(0.95f, 1.11f);
        }
        sound.Play();
    }    
}
