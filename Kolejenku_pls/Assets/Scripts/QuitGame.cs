using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Basic script to terminate the game
/// </summary>
public class QuitGame : MonoBehaviour
{
    public Button quitButton;

    // Start is called before the first frame update
    void Start() {
        quitButton.onClick.AddListener(Quit);
    }

    private void Quit() {
        Application.Quit();
    }
}
