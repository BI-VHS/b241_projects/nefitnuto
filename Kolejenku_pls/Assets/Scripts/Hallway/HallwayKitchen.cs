using UnityEngine;

public class HallwayKitchen : MonoBehaviour {
    [SerializeField] private GameObject table;
    [SerializeField] private GameState gameState;

    private void Start() {
        table.SetActive(false);

        // find game state object, or return
        GameObject stateObj = GameObject.FindGameObjectWithTag("GameState");
        if (stateObj == null) return;
        gameState = stateObj.GetComponent<GameState>();

        // if skibidi has toilet in kitchen, then tunak tunak tun in hallway
        if (gameState[STATE.SKIBIDI_ALLOW] != 0 && gameState[STATE.INDIANS_ALLOWED] > 6) {
            table.SetActive(true);
        }
    }

    // temp, to be deleted
    private void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            table.SetActive(!table.activeInHierarchy);
        }
    }
}
