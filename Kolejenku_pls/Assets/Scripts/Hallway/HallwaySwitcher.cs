using UnityEngine;

public class HallwaySwitcher : MonoBehaviour {
    [SerializeField] GameObject[] hallwayParts;
    [SerializeField] private int hallwayID = 0;

    public void Start() {
        foreach (GameObject part in hallwayParts)
            part.SetActive(false);

        if (hallwayID < 0)
            hallwayID = 0;

        if (hallwayID >= hallwayParts.Length)
            hallwayID = hallwayParts.Length - 1;

        hallwayParts[hallwayID].SetActive(true);
    }

    public void goLeft() {
        --hallwayID;
        Start();
    }
    
    public void goRight() {
        ++hallwayID;
        Start();
    }
}
