using UnityEngine;

public class HallwayBomb : MonoBehaviour {
    [SerializeField] private GameObject bomb;
    [SerializeField] private GameObject poster;
    [SerializeField] private GameState gameState;

    private void Start() {
        bomb.SetActive(false);
        poster.SetActive(false);

        // find game state object, or return
        GameObject stateObj = GameObject.FindGameObjectWithTag("GameState");
        if (stateObj == null) return;
        gameState = stateObj.GetComponent<GameState>();

        // if VSE student is allowed in, he will commit terrorism
        if (gameState[STATE.VSE_ALLOW] != 0) {
            bomb.SetActive(true);
        }

        // if Punjab > 3, then praise bomb
        // proc tohle vubec delame? nejak nevidim spojitost bomba a ind?
        if (gameState[STATE.INDIANS_ALLOWED] > 3) {
            poster.SetActive(true);
        }
    }

    // temp, to be deleted
    private void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            bomb.SetActive(!bomb.activeInHierarchy);
        }

        if (Input.GetKeyDown(KeyCode.Return)) {
            poster.SetActive(!poster.activeInHierarchy);
        }
    }
}
