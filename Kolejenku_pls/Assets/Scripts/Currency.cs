using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Currency : MonoBehaviour {
    public int victoryMoneySum = 0;
    public int defeatMoneySum = 0;
    private int currentMoneySum;

    public void Start() {
        currentMoneySum = 0;
    }

    public void UpdateCurrentMoneySum(int update) {
        if(InGame()) {
            currentMoneySum += update;
        }
    }
    
    public bool InGame() {
        return currentMoneySum < victoryMoneySum && currentMoneySum > defeatMoneySum;
    }

    public bool HasWon() {
        return currentMoneySum >= victoryMoneySum;
    }

    public bool HasLost() {
        return currentMoneySum <= defeatMoneySum;
    }

    public int GetCurrentMoneySum() {
        return currentMoneySum;
    }
}
