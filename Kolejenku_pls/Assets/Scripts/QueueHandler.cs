using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;


/// <summary>
/// Handler of all people in the queue
/// </summary>
public class QueueHandler : MonoBehaviour {

    /// <summary>
    /// Defined movement of everyone in the queue
    /// </summary>
    [SerializeField] private GameObject prefabMovement;
    /// <summary>
    /// Lenght of the queue
    /// </summary>
    [SerializeField] private int amountOfPeople;
    /// <summary>
    /// Movement speed while approaching doors
    /// </summary>
    [SerializeField] private float movementSpeed;
    /// <summary>
    /// Checkpoints where people change the direction of travel
    /// </summary>
    [SerializeField] private List<GameObject> waypoints;
    /// <summary>
    /// Object which holds all the leaving people, previously queued for entry
    /// </summary>
    [SerializeField] private GameObject leavingQueue;
    /// <summary>
    /// Indicator, if people in queue can move or not
    /// </summary>
    private bool allowNextToEnter;
    /// <summary>
    /// Indicator, that person has got inside
    /// </summary>
    private bool spawnPerson;

    // Start is called before the first frame update
    void Start() {
        allowNextToEnter = true;
        spawnPerson = false;
        for (int i = 0; i < amountOfPeople; ++i) {
            GameObject newChild = Instantiate(prefabMovement);
            newChild.name = "Queue" + i;
            newChild.transform.SetParent(transform);
            Vector3 newPosition = waypoints[0].transform.position;
            newPosition.x -= i;
            newChild.transform.position = newPosition;
            QueuedPerson person = newChild.AddComponent<QueuedPerson>();
            person.SetWaypoint(waypoints[0].transform.position);
        }
    }

    /// <summary>
    /// Changes target's old waypoint for a new one 
    /// </summary>
    /// <param name="currentChild"> currently selected person </param>
    public void UpdateNextWaypoint(GameObject currentChild) {
        QueuedPerson person = currentChild.GetComponent<QueuedPerson>();
        int currentIndex = person.GetWaypointIndex();
        // Letting next person to enter
        if (currentIndex == waypoints.Count - 2 && allowNextToEnter) {
            person.SetWaypointIndex(++currentIndex);
            person.SetWaypoint(waypoints[currentIndex].transform.position);
            return;
        }
        // Reset person back to start
        if (currentIndex == waypoints.Count - 1) {
            spawnPerson = true;
            allowNextToEnter = false;
            MoveChildToStart(currentChild, person);
            return;
        }
        person.SetWaypointIndex(++currentIndex);
        person.SetWaypoint(waypoints[currentIndex].transform.position);
    }


    // Update is called once per frame
    void Update() {
        if (!allowNextToEnter) return;
        // Update all object children
        for (int i = 0; i < transform.childCount; ++i) {
            GameObject currentChild = transform.GetChild(i).gameObject;
            QueuedPerson queuedPerson = currentChild.GetComponent<QueuedPerson>();
            //Change movement to next waypoint
            if (currentChild.transform.position == queuedPerson.GetCurrentWaypoint()) {
                UpdateNextWaypoint(currentChild);
            }
            currentChild.transform.position = Vector3.MoveTowards(currentChild.transform.position, queuedPerson.GetCurrentWaypoint(), movementSpeed * Time.deltaTime);
        }
    }

    /// <summary>
    /// Resets target position back to the end of the queue when it has succesfully reached the doors
    /// </summary>
    /// <param name="target"> specific person in the queue </param>
    /// <param name="queued"> target's component </param>
    private void MoveChildToStart(GameObject target, QueuedPerson queued) {
        queued.SetWaypointIndex(0);
        queued.SetWaypoint(waypoints[0].transform.position);
        target.transform.position = queued.GetNewStartingPoint();
    }

    /// <summary>
    /// Everyone in queue starts moving once again and movement animation is triggered
    /// </summary>
    public void StartMovingEveryoneInQueue() {
        if(transform.childCount == 0) return;
        foreach (Transform child in transform) {
            GameObject queuedPerson = child.gameObject;
            QueuedPerson person = queuedPerson.GetComponent<QueuedPerson>();
            person.ChangeMovementAnimation();
        }
        allowNextToEnter = true;
    }

    /// <summary>
    /// Stops everyone in queue, and idle animation is triggered
    /// </summary>
    public void StopMovingEveryoneInQueue() {
        foreach (Transform child in transform) {
            GameObject queuedPerson = child.gameObject;
            QueuedPerson person = queuedPerson.GetComponent<QueuedPerson>();
            if (person == null) continue;
            person.ChangeMovementAnimation();
        }
    }

    public void MakeEveryoneLeave() {
        int objCounter = 0;
        int x = transform.childCount - 1; 
        for (; x >= 0; x--) {
            // Create new leaving person instance
            GameObject newLeavingPerson = Instantiate(prefabMovement, leavingQueue.transform);
            newLeavingPerson.name = "Leaving Person" + objCounter++;

            Transform child = transform.GetChild(x);
            Vector3 childPosition = child.position;
            LeavingPerson currentObj = newLeavingPerson.AddComponent<LeavingPerson>();
            newLeavingPerson.transform.position = childPosition;
            // Get the nearest waypoint
            int index = 0;
            float currentClosestDistance = float.MaxValue;
            for (int i = 0; i < waypoints.Count; ++i) {
                if (waypoints[i].transform.position.x > childPosition.x) continue;
                // Calculate closest waypoint
                float distance = math.abs(child.position.x - waypoints[i].transform.position.x);
                if (distance < currentClosestDistance) {
                    currentClosestDistance = distance;
                    index = i;
                }
                currentObj.waypoints.Insert(0, waypoints[i]);
            }
            currentObj.movementSpeed = movementSpeed;
            currentObj.SetWaypoints(index, waypoints[index]);

            DestroyImmediate(transform.GetChild(x).gameObject);
        }
    }          


    public bool SpawnPerson() { return spawnPerson; }
    public void SetSpawnPerson(bool value) { spawnPerson = value; }
    public bool AllowNextToEnter() { return allowNextToEnter; }
    public void SetAllowNextToEnter(bool value) {  allowNextToEnter = value; }
}
