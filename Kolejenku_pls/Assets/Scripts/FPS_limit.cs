using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPS_limit : MonoBehaviour {
    public enum FPSLimit {
        limit30 = 30, limit60 = 60, limit120 = 120
    }

    public FPSLimit limit;

    void Start() { 
        Application.targetFrameRate = (int)limit;
    }
}
