using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgreementFunction : MonoBehaviour
{
    public Sprite without_signature_small;
    public Sprite without_signature_full;
    public Sprite with_signature_small;
    public Sprite with_signature_full;
    public PaperSpriteChanger sprite_switcher;
    public SpriteRenderer sprite;
    private bool signed = false;
    public AudioSource sound;

    public GameObject pen;

    public PeopleAtDesks people;
    
    void Start()
    {
        pen = GameObject.Find("Pen");
    }

    void Update()
    {
        if
        (
            !signed && sprite_switcher.spriteType == PaperSpriteChanger.SpriteType.FULL
        && pen.GetComponent<BoxCollider2D>().bounds.Intersects(gameObject.GetComponent<BoxCollider2D>().bounds)
        )
        {
            sound.Play();
            sprite_switcher.full_sprite = with_signature_full;
            sprite_switcher.small_sprite = with_signature_small;
            sprite.sprite = with_signature_full;
            signed = true;
            PersonToCheck person = people.GetCurrentPerson().GetPerson();
            person.GetGameObjectDocuments().Add(gameObject);
        }
    }

    public bool isSigned()
    {
        return signed;
    }
}
