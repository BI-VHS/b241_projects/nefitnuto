using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageTurner : MonoBehaviour
{
    public GameObject[] pages;
    int current_page_number = 0;

    public void NextPage()
    {
        pages[current_page_number].SetActive(false);
        current_page_number++;
        pages[current_page_number].SetActive(true);
    }
    
    public void PrevPage()
    {
        pages[current_page_number].SetActive(false);
        current_page_number--;
        pages[current_page_number].SetActive(true);
    }
}
