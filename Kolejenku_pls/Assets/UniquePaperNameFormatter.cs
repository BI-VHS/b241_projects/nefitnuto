using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniquePaperNameFormatter : MonoBehaviour
{
    // Start is called before the first frame update
    public void ReformatPaper()
    {
        print("im here");
        var person = gameObject.GetComponent<PersonToCheck>();
        List<Paper> papers = new List<Paper>();
        foreach(var paper in person.documents)
        {
            if(paper is Passport)
            {
                papers.Add((Passport)paper);
            }
            if(paper is IdCard)
            {
                papers.Add((IdCard)paper);
            }
        }

        foreach(var paper in papers)
        {
            var split_name = paper.name.Split();
            paper.name = "";
            for(int i = 0; i < split_name.Length; ++i)
            {
                paper.name += split_name[i];
                if(split_name[i] != "")
                {
                    paper.name += '\n';
                }
            }
        }
    }
}
