using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuSwitcher : MonoBehaviour
{
    public GameObject load_menu;
    public GameObject main_menu;
    public GameObject credits_menu;
    
    private GameObject current_menu;

    void Start()
    {
        current_menu = main_menu;
    }

    public void LoadGame()
    {
        current_menu.SetActive(false);
        load_menu.SetActive(true);
        current_menu = load_menu;        
    }

    public void Credits()
    {
        current_menu.SetActive(false);
        credits_menu.SetActive(true);
        current_menu = credits_menu;     
    }

    public void MainMenu()
    {
        current_menu.SetActive(false);
        main_menu.SetActive(true);
        current_menu = main_menu;    
    }
}
