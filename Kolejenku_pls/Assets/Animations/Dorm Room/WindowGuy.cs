using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class WindowGuy : MonoBehaviour, IPointerClickHandler {
    [SerializeField] int clickCount = 0;
    [SerializeField] float time = 10f;

    public void OnPointerClick(PointerEventData eventData) => ++clickCount;

    private void Update() {
        if (clickCount == 10) {
            clickCount = 0;
            int quoteID = Random.Range(0, gameObject.transform.childCount - 1);
            StartCoroutine(showQuote(quoteID));
        }
    }

    IEnumerator showQuote(int quoteID) {
        gameObject.transform.GetChild(quoteID).gameObject.SetActive(true);
        yield return new WaitForSeconds(time);
        gameObject.transform.GetChild(quoteID).gameObject.SetActive(false);
    }
}
