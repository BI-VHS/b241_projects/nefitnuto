using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvestigationNode : MonoBehaviour
{
    public enum Type
    {
        NAME,
        STAMP,
        DATE,
        BLOK_NUMBER,
        ID,
        PHOTO
    }

    public Type type;
    public Sprite active_sprite;
    public Sprite inactive_sprite;
    public SpriteRenderer spriteRenderer;

    private bool is_active;

    void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    public void SetActive(bool value)
    {
        if(value == is_active)
        {
            return;
        }

        // ? animation would be nicer, i think
        spriteRenderer.sprite = value ? active_sprite : inactive_sprite;
    }
}
