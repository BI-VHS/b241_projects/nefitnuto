### Hubení švábů
Hráč má za úkol zabít všechny šváby pobíhající po kuchyňce. Za každého zabitého švába je mu uděleno finanční ohodnocení a za každého nezabitého švába naopak strhnutí částky ze mzdy. Na zabití všech švábů má časový limit.

### Hubení štěnic
Hráč má za úkol zabít všechny štěnice pobíhající po pokoji. Za každou zabitou štěnici je mu uděleno finanční ohodnocení a za každou nezabitou naopak strhnutí částky ze mzdy. Na zabití všech štěnic má časový limit. Štěnice se na rozdíl od švábů dokážou na chvíli schovat. 

### Uklízení záchodů
Hráč má za úkol uklidit zašpiněné záchody. Stav dveří je však velmi špatný a každé jejich otevření a zavření zvětšuje jejich poškození. Dvě kabinky ze tří vždy potřebují uklidit, ale hráč dopředu neví, které dvě kabinky to jsou a musí tak náhodně vybírat. Za otevření dveří čistého záchodu jsou mu strhnuty peníze. Špinavé kabinky pak hráč uklízí poskytnutým hadrem.

### Zabavování kontrabandů
Hráč má za úkol projít pokoj studenta a najít nebezpečné či nelegální předměty a zabavit je. Po otevření úložných prostorů v pokoji jsou hráči vždy nabídnuty 3 předměty ze kterých musí vybrat předměty, které chce zabavit nebo kliknout na křížek v případě kdy si myslí, že jsou všechny předměty v pořádku.