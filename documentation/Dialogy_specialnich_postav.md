### Bomb Expert:

Příchod:
- Dobrý den, pustíte mě do-
- BOMBA!!!
- Holy... To je opravdická bomba?
- No asi ano!!
- Týjo... Ale vypadá, že to dělal nějakej amatér.
- Hele víte vy co? Já jsem si zapomněla kolejenku, ale pokud mě pustíte...
- No to nevím! Prosím tebe přestaň řešit blbosti, tady je BOMBA!!
- ...pak bych vám mohla dát svoji knížku o bombách.
- ...
- A má ta knížka nějaké informace na zneškodnění bomby?
- Má.
  
- Vpuštení:
- Tam běžte no!
- Děkují. Tady máte tu knížku!
- Odmítnutí:
- Nemůžu vás pustit, neotravujte!
- Jak myslíte. Já chtěla jenom pomoct.
- Já žádnou pomoc nepotřebuju. Co bych to byla?
- Co? Co bych to byla? Kdybych neuměla se postarat o nějakou "bombu"

### Metro týpek:

Příchod:
- Já si ty boty prostě sundám… Joo dobrý den, já tady totiž bydlím, víš to?
- Kolejen-
- Než něco řeknete, tak bych chtěl konstatovat, že já jsem tady jedineeej střízlivej.
- Potřebuju dovnitř si uklidit boty.
- Vy jste bos!
- No víte, mě to totiž takhle dělá dobře.
- ...
- Nechceme si podat ruce?

Vpuštění:
- Tak běžte.
- Gratuluji. Však jsem vám říkal, že jsem úplně v pohodě.

Odmítnutí:
- Běžte s těma páchnoucíma botama pryč!
- A kam si je teda podle tebe mám dát?!
- Pro mě za mě pod Nuselák...
- Myslíte si, že pod Nuselákem mě jako přijmou?
- Já si tam ty boty prostě sundám a co pak?
- Chtěl jsem vám dát žvýkačku, ale asi z toho nic nebude


### SUZ inspektor
Příchod:
- Zdravím
- Jenom jsem vás přišel uvítat do vaší nové práce. Nebojte, je to pohodka.

Odchod:
- No, já musím běžet, tak se mějte. A pevné nervy!

### Skibidi kid:
**Den 1:**
Příchod:
- Tak jsem to tady přišel rizznout. Tady to bude hrozně lit.
- Btw na takovou babu jseš fakt pookie.
- No přestanu yappovat, ať to tady můžeme dokončit a já můžu jít shánět nějakej čaj.

Vpuštění:
- Gyat, tohle bude hittovat jináč. Seš fakt goat.

Odmítnutí:
- Tak to je cringe. Co teď mám jako podle tebe dělat?
- Děláš tady absolutní dog water, skibidi ohio rizz.

**Den 2:**
Příchod:
- Takže minule si mě tady totálně hard L-ovala, ale dneska už ne.
- Jestli nechceš mínusovou auru, tak mě pustíš..
- Rizznul jsem tady půlku Strahova, tak dělej nebo svolám všechny NPC na backup a projdu i bez tebe.

Vpuštění:
- No konečně nějaký W, teď už jenom rizznout zbytek Strahova a budu mít max auru B).

Odmítnutí:
- Ty pičo, takže další masivní L, jo?
- Ty máš takový skill issue, to mi chceš jako dát shadowban irl?
- Sis spletla asi levé, ne? Akorát tady děláš speedrun do cringe zóny?

### VŠE student:
**Den 1:**
Příchod:
- Brej den.
- Nazdar
- Vy ste tu nová? No nevadí, já už tu jsem od prvopočátku!
- Ani to nemusíte kontrolovat, nechci vám přidělávat práci.
- Hm

Vpuštění:
- Můžete dovnitř.
- Díky! Pěkný zbytek dne vám přeji! :)

- Odmítnutí:
- Zmizte mi z očí.
- Já se vrátím.
- Až mě příště nepustíte, budete litovat.
- Už se bojím...

**Den 2:**
Příchod:
- Kolejenku, prosím.
- Tak jsem se vrátil.
- To vidím.
- Doufám, že nebudou žádné problémy. To by bylo nemilé.

Vpuštění:
- Překvapivě to vypadá, že je vše v pořádku.
- Já věděl, že jste rozumná ženská.

Odmítnutí:
- Zmizte.
- Já vás varoval, že toho budete litovat.