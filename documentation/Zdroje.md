#### Drtivá většina tvorby použitá ve hře je vytvořená naším týmem.

#### Zdroje, které jsme čerpali odjinud:

**Animace**

Animace stojících postav: [Papers, please.](https://papersplea.se/)

**Hudba a zvukové efekty:**

Hudba v kanceláři: [Fesliyanstudios](https://www.fesliyanstudios.com/royalty-free-music/download/slavic-drinking-song/2060)

Hudba v menu: [Steve Oxen - Slavic Lo-Fi](https://www.youtube.com/watch?v=gtkuo-oNqz8)

Ostatní ozvučení: 

- [Fesliyanstudios](https://www.fesliyanstudios.com)
- [Zapsplat](https://www.zapsplat.com/)
- [Freesound](https://freesound.org/)
