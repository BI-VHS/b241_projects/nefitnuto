## Zasazení
Ve hře se ujmete role "kolejbáby" na jednom z bloků Strahovských kolejí. Cílem hry je získat dostatek peněz na vysněnou dovolenou a zároveň udržení stavu kolejí na přijatelné úrovni.

Hlavní výplní hry je kontrolování kolejenek studentů (ala Papers, please), kteří se chtějí dostat dovnitř bloku, a ubytovávání studentů nových. Po skončení šichty u vrátnice se ještě bude muset vyřešit nějaký problém na koleji ve formě klikací mini-hry. Nezvládnutí mini-hry znamená strhnutí peněz z platu a zhoršení stavu kolejí.

## Scény
![](./img/diagram_scene.png){height=50%}

## Vrátnice
Zde hráč stráví nejvíce času. Během pracovní doby postupně přicházejí NPC a do okénka dávají svoje dokumenty *[1]*. Tyto dokumenty jsou buď kolejenka, nebo lejstra nutná k registraci na kolej. Aby je hráč mohl zkontrolovat musí je přesunout na svoji pracovní desku *[2]*. Na zdi kanceláře *[3]* se nacházejí objekty obsahující informace vhodné ke kontrole dokumentů.

![](./img/vratnice_marked.png){width=50%}

## Mini-hry

### Pokoj
Cílem bude hledat zrovna ilegální kontrabant.

![](../sprites/Room/roomBackground.png){width=50%}

### Kuchyňka
Cílem bude uklidit prostředí a zbavit se potenciálních škůdců.

![](../sprites/Kitchen/kitchenBackground.png){width=50%}

### Záchody

![](../sprites/Toilets/toiletsBackgroundTemp.png){width=50%}

## Unikátnost

### Postavy

Ve hře se bude nacházet několik unikátních postav, které se budou na vrátnici opakovaně vracet. Vpuštění některých unikátních postav na kolej dále me ovlivnit situace u miniher, či ending. Mezi tyto postavy patří:

#### SÚZ inspektor

Tato postava se bude opakovaně vracet a hodnotit práci hráče. Může ho odměnit za správně vykonané úlohy nebo naopak potrestat pokud hráč pouští na kolej studenty s neplatnými či padělanými doklady. Při dobře vykonané práci může hráč obdržet unikátní předměty pro vystavení na zeď. 

#### VŠE student

Student VŠE se opakovaně bude snažit dostat na kolej. Ná zakládě rozhodnutí hráče, zda studenta pustí či nepustí na škole, tento student spustí jedinečné události ve hře.

#### Skibidy kid

Student zastupující největší stereotypy mladší generace. Po vpuštění tohoto studenta na kolej dojde k unikátním interrakcím v rámci miniher. 

#### Metro týpek

Unikátní postava jež je referencí na událost jež nastala bezprostředně po týmovém meetingu. Postava může hráči nabídnout unikátní itemy. 


### Doklady

Ve hře se hráč, kromě náhodně generovaných dokladů může setkat i s unikátními doklady. Zejména tak půjde o doklady unikátních postav. Nepůjde tak o běžné neplatné údaje, ale o celkově padělané dokumenty jako takové. 
