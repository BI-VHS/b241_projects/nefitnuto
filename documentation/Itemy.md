### Pero
Slouží k podepisování smlouvy o ubytování

![pero](img/pero.png){width=7%}

### Občanský průkaz
Slouží k identifikaci studenta. Obsahuje jméno, příjmení, fotku, platnost a id studenta. Ve hře se vyskytují dva typy: občanský průkaz české republiky a občanský průkaz slovenské republiky. Všechny údaje na občanském průkazu mohou být falešné.

![obcanka](img/obcanka.png){width=20%}

### Pas
Slouží k identifikaci zahraničních studentů. Obsahuje jméno, příjmení, fotku, platnost, id a zemi původu studenta. Všechny údaje na pasu mohou být falešné.

![pas](img/pas.png){width=18%}

### Potvrzení o studiu
Slouží k možnosti ubytování se na kolej. Obsahuje jméno, příjmení, platnost, id a razítko s podpisem. Všechny údaje na potvrzení o studiu, včetně razítka mohou být falešné.

![studium](img/studium.png){width=18%}

### Kolejenka
Slouží ke vstupu studenta na kolej. V případě ubytování nového studenta je pro něj vytisknuta nová kolejenka. Obsahuje jméno, příjmení, fotku, číslo boku, číslo pokoje, jméno fakulty a razítko. Všechny údaje včetně razítka mohou být falešné.

![kolejenka](img/kolejenka.png){width=18%}

### Smlouva o ubytování
Slouží k ubytování studenta na koleji. V případě přijmutí studenta musí být podepsána perem a předána studentovi spolu se všemi poskytnutými doklady.

![smlouva](img/smlouva.png){width=18%}

### Pokuta
Pokuta je hráči vystavena v případě, kdy přijmul na kolej studenta s nesprávnými údaji anebo naopak odmítnul studenta se správnými údaji. První pokuta, kterou hráč obdrží je varovná a nejsou mu za ni strhnuty peníze z platu. V případě opakování chyb už se peníze strhávají.

![pokuta](img/pokuta.png){width=18%}

### Leták s návodem
Hráči je ve hře k dispozici "leták správné kolejbáby", ve které dostává informace o tom, které studenty může a nemůže vpouštět na kolej. Obsahuje i znázornění správných razítek. Jakákoliv jiná varianta razítek, která není uvedená v letáku je neplatná. 

![letak](img/letak.png){width=18%}

### Bomba
Na základě hráčových rozhodnutí může dojít k události, kdy hráč bude muset zneškodnit bombu. Bomba obsahuje 4 šroubky, které musí být odstraněny, následně hráč musí přestřihnout 4 drátky ve správném pořadí. V případě, kdy hráč zvolí špatné pořadí bomba vybouchne a nastává konec hry. 

![bomba](img/bomba.png){width=18%}

### Příručka o bombách
V případě, kdy nastane událost s bombou poskytne studentka hráčovi svou příručku pro zneškodnění bomb, podle které může hráč bombu zneškodnit. 

![prirucka](img/prirucka.png){width=18%}

### Hadr
Hadr slouží hráči k uklízení záchodů. Je mu k dispozici pouze na toaletách.

![hadr](img/hadr.png){width=6%}