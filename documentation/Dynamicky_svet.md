# Animace

Celá hra je vytvářena v 2D pixel art stylu. Veškeré animace tak spočívájí ve střídání spritů a práce s unity animátorem.

## Animace modelu

Postavy u překážky mají základní idle dýchání. 

Předměty, které lze v pokoji a jiných oblastech nakliknout mají fáze zavřeno/otevřeno.

Vrána létá po obloze.

## Denní cyklus

Ve hře se pracuje s denním cyklem, jelikož má hráč omezenou dobu na to aby ubytoval studenty. V kanceláři tam může vidět točící se hodiny a v pozdějších hodinách se i začne rozsvicovat jednotlivá okna.

## Cyklické činnosti

**Studenti chystájící se ubytovat:** přicházejí - čekají - nastupují do konceláře - odcházejí

**Student v okně:** přichází - mává - zalévá kytku - odchází

**Studenti na pokojích:** hlasitá hudba (kalba) - bitka - zvracení na záchodech - klid (spánek)

![](./img/nastup.gif)

![](./img/typekvokne.gif)

![](./img/Cykly.png){height=50%}

## Produkční cykly

**Špinavé nádobí:** V případě kdy je na kolej ubytováno více než n studentů z Indie. Začnou v kuchyni společně vařit a produkovat špinavé nádobí. S počtem studentů roste i počet špinavého nádobí.
<br>
<img src="img/kitchen0.png" alt="drawing" width="275"/>
<img src="img/kitchen1.png" alt="drawing" width="275"/>
<img src="img/kitchen2.png" alt="drawing" width="275"/>
<img src="img/kitchen3.png" alt="drawing" width="275"/>
<br>


**Hnědý kód:** V případě kdy je na kolej ubytována unikátní postava Skibidi kid. Začne produkovat v různých lokalitách objekty "hnědé barvy". 

**Bomba:** V případě ubytování VŠE studenta na kolej tento student vytvoří v určitý okamžik na chodbě bombu.

## Chování

1. **Vaření na chodbě:** K tomuto chování dochází když hráč ubytuje na kolej postavu Skibidi kid a určitý počet studentů z Indie. V případě Skibidi kid začne produkovat objekty v různých lokalitách včetně kuchyně a hráč tyto objekty neodklidí, nahněvá to Indické studenty. Ti v takovém případě začnou vařit na chodbě, jelikož jim vadí v jakém stavu je kuchyně. V případě, že stav kuchyně napravíme, tak studenti opět začnou vařit v kuchyni.
2. **Uctívání bomby:** V případě kdy je na chodbě umístěna bomba VŠE studentem a není s ní nijak manipulováno, tak Indové tuto bombu začnou uctívat jelikož si pomýlí hákový kříž s hinduistickou svastikou.
3. **Švábi:** Procházejí se a když narazí na potravu začnou jíst, v případě kdy se hráč přiblíží myší aby je zabil, začnou rychle utíkat. V případě kdy nestihnout utéct a hráč je zabije, rozprsknou se.
4. **Štěnice:** Procházejí se a když najdou skulinku, na chvíli se do ní schovají. V případě kdy se hráč přiblíží myší, aby je zabil, tak začnou rychle utíkat. V případě kdy nestihnout utéct a hráč je zabije, rozprsknou se.

![](./img/svab.gif)

![](./img/complex.png){height=50%}



## Unikáty

Pokud hráč bude mnohokrát klikat na studenta v okně, tak mu tento student odsekne ať ho nechá být. 

Mezi postavamy, které hráč může na kolej přijmout, existuje vzácný student se speciálním kloboukem. Šance na jeho objevení je velice malá.