## Mechaniky

Práce hráče je kontrola dokumentů studentů, kteří se chtějí buďto ubytovat na kolej anebo vstoupit do svých pokojů. Činí tak kontrolou specifických dokumentů, kde musí hlídat, jestli jsou všechny informace správné. Na základě kontroly pak studenta buďto na kolej vpustí anebo nikoli.

## Příběh
Hráč po úmorném výběrovém řízení získává práci snů kolejbáby na Strahovských kolejích. Jeho cílem je našetřit si na vysněnou dovolenou na Kanárských ostrovech. Musí tak nejen kontrolovat studenty kolejí, ale aktivně se snažit zneškodnit štěnicový mor, hovnového démona nebo potlačit teroristické skupiny. Stihne si hráč našetřit na vysněnou dovolenou nebo se po šílených událostech na Strahovských kolejích jeho osud vydá jiným směrem?

## Pravidla dnů
Na základě dni v měsíci a dění ve světě jsou pro každý den vytvořena pravidla, podle kterých hráč přijímá studenty. Tyto pravidla jsou blíže popsány [zde](https://gitlab.fit.cvut.cz/BI-VHS/b241_projects/nefitnuto/-/blob/main/documentation/pravidla.md).


## Dialogový systém
Ve hře není přímá možnost výběru dialogových možností. Hráč tyto možnosti vybírá nepřímo skrze akce, které provádí např. přijmutí/odmítnutí studenta. Obyčejné postavy mají vytvořené sety odpovědí, pro jednotlivé události, které mohou nastat např. falešné razítko nebo špatné číslo bloku na kolejence. 


## Dialogy
Při příchodu je každý student vyzván k předložení kolejenky či jiných papírů potřebných pro ubytování. Na základě rozhodnutí hráče pak student reaguje jednou z mnoha možností odpovědi, pro nastalou situaci. 

**Příklady reakcí**

Špatné číslo bloku:
- To jste se určitě přehlídla.
- Ááá chybička se vloudila.

Přijmutí studenta:
- Skvělý, jen zrychlíme příště tempíčko, jo?
- Nejpřívětivější vrátná, co jsem tu kdy potkal.


## Dialogy speciálních postav
Ve hře se vyskytuje několik speciálních postav, s unikátními dialogy. Většina z těchto postav má dále dopad na hru, podle rozhodnutí hráče. Tyto dialogy mají jednoznačné odpovědi na různé typy událostí.

Příklad úvodního dialogu SUZ inspektora:
- Jenom jsem vás přišel uvítat do vaší nové práce. Nebojte, je to pohodka.
- No, já musím běžet, tak se mějte. A pevné nervy!

[Dialogy ostatních postav](https://gitlab.fit.cvut.cz/BI-VHS/b241_projects/nefitnuto/-/blob/main/documentation/Dialogy_specialnich_postav.md)

## Úkoly a předměty

Hlavním úkolem hráče je kontrola dokladů studentů a následné přijetí/odmítnutí. Po každém dni pak hráč musí splnit přidělenou minihru za jejichž splnění obdrží finanční ohodnocení. Jednotlivé minihry jsou popsány [zde.](https://gitlab.fit.cvut.cz/BI-VHS/b241_projects/nefitnuto/-/blob/main/documentation/Minihry.md)

Deska kanceláře slouží hráči jako jakýsi inventář. Na desce hráč manipuluje s veškerými dokumenty studentů, které kontroluje. Taky zde dochází k vytváření nových dokumentů jako je smlouva o ubytování nebo vytisknutí kolejenky pro studenta. Rozpis veškerých dokumentů a ostatních předmětů [zde.](https://gitlab.fit.cvut.cz/BI-VHS/b241_projects/nefitnuto/-/blob/main/documentation/Itemy.md)