## HLAVNI POSLOUPNOST

### První den
- **Kolejenka:** true
	- kontroluju cislo bloku, pokoj, fotku a razítko
- **Pas:** false
	- neni
- **Občanka:** false
	- neni
- **Ubytovaní:** false
	- neni 
- **Příběh:**
	- Začíná semestr, obyvatelé Strahova z minulých ročníků se začínají vracet a očekávají vpuštění na své zahnilé pokoje. Byli jste zvoleni jako správce bloku 13. Prozatím nepouštějte nikoho, kdo není z vašeho bloku. Přesto, že semestr sotva začíná, už i teď se nám objevují falšované kolejenky, je potřeba si dát pozor na razítka.

### Druhý den:
- **Kolejenka:** true
	- kontroluju číslo bloku, pokoj, fotku a razítko
- **Občanka:** true
	- platnost
- **Pas:** true
	- platnost
- **Ubytovaní:** true
	- skrz občanku/pas jméno, příjmení a národnost
- **Příběh:** 
	- Nově vás čeká možnost ubytovávat nově příchozí. U nich je důležité zkontrolovat, jestli sedí údaje vyplněné na papíře skrz občanku nebo pas. Stále dávejte pozor na padělky.

### Treti den:
- **kolejenka:** true
	- kontroluju číslo bloku, pokoj, fotku a razítko
- **Občanka:** true
	- platnost
- **Pas:** true
	- platnost
- **Ubytovaní:** true
	- skrz občanku/pas jméno, příjmení a národnost
- **Příběh:**
	- Dále jsme vám schválili infoschůzku, můžete tedy pouze dnes pouštět lidi i z jiných bloků ať si také užijou. Žádáme vás, ať nově kontrolujete občanky všem, padělky se nám rychle rozšiřujou a kontrolovat jenom razítko už nestačí. Trvale také můžete přijímat studenty z bloků 10 a 11 kvůli navázané spolupráci místních alkoholiků.

### Čtvrtý den:
- **kolejenka:** true
	- kontroluju číslo bloku, pokoj, fotku a razítko
- **Občanka:** true
	- platnost
- **Pas:** true
	- platnost
- **Ubytovaní:** true
	- skrz občanku/pas jméno, příjmení a národnost
- **Příběh:** 
	- VYPUKLA VÁLKA! Slovensko přes noc překročilo hranice a zaútočilo na Brno. Bohužel pro ně, jejich technika byla ještě z doby kamenné a teď si jejich území nárokuje Maďarsko. Za žádných okolností tyto obyvatele Severního Maďarska nepouštějte dovnitř. Kvůli utkání v alko autobusu mezi naším blokem a blokem 7, můžete pouštět i ubytované ze sedmého bloku.

### Páty den:
- **kolejenka:** true
	- kontroluju cislo bloku, pokoj, fotku a razítko
- **Občanka:** true
	- platnost
- **Pas:** true
	- platnost
- **Ubytovaní:** true
	- nic
- **Příběh:**
	- Kapacity pro nově příchozí studenty už došli, pokud za vámi někdo přijde, že chce ubytovat, můžete ho rovnou poslat pryč. Nepouštět nikoho ze Slovenska stále platí. Získali jsme také informace, že máte zamořené celé 3. patro štěnicemi. Kvůli nevyžádaným návštěvám hygieny radši nikoho z toho patra nepouštějte.

### Šestý den:
- **kolejenka:** true
	- kontroluju cislo bloku, pokoj, fotku a razítko
- **Občanka:** true
	- platnost
- **Pas:** true
	- platnost
- **Ubytovaní:** true
	- skrz občanku/pas jméno, příjmení a národnost
- **Příběh:**
	- S radostí vám oznamujeme, že můžete znovu začít pouštět občany Slovenska. Ukázalo se, že se jednalo o nezávislou skupinu teroristu z východního Slovenska.  Dále díky zamořenému 3. patru se nám uvolnila kapacita pokojů, můžete tedy znovu přijímat nově příchozí studenty.


### Sedmý den:
- **kolejenka:** true
	- kontroluju cislo bloku, pokoj, fotku a razítko
- **Občanka:** true
	- platnost
- **Pas:** true
	- platnost
- **Ubytovaní:** true
	- skrz občanku/pas jméno, příjmení a národnost
- **Příběh:**
	- Máme podezření, že 4. a 5. patro obsadila rumunská mafie a pronajímají povlečeni o 50% levněji než SÚZ. Tímto vás žádáme, ať nikoho z těchto pater nepouštíte. Rušíme povolení pouštět studenty z 10. bloku, protože na 2. patře ukradli žárovku. Studenty z 11. bloku můžete stále pouštět.

### Osmý den:
- **kolejenka:** true
	- kontroluju cislo bloku, pokoj, fotku a razítko
- **Občanka:** true
	- platnost
- **Pas:** true
	- platnost
- **Ubytovaní:** true
	- skrz občanku/pas jméno, příjmení a národnost
- **Příběh:**
	- Jelikož dnes probíhá Holi, slavnost barev, tak vás prosíme, ať dnes nepouštíte žádné Indy, kvůli zachování klidu a pořádku. Dále máte povoleno pouštět studenty z bloku 4, 7 a 9 za účelem využití naší studovny."

### Deváý den:
- **kolejenka:** true
	- kontroluju cislo bloku, pokoj, fotku a razítko
- **Občanka:** true
	- platnost
- **Pas:** true
	- platnost
- **Ubytovaní:** true
	- skrz občanku/pas jméno, příjmení a národnost
- **Příběh:**
	- Svátek Holi jsme úspěšně přežili, znovu můžete pouštět Indy.

### Desátý den:
- **kolejenka:** true
	- kontroluju cislo bloku, pokoj, fotku a razítko
- **Občanka:** true
	- platnost
- **Pas:** true
	- platnost
- **Ubytovaní:** true
	- skrz občanku/pas jméno, příjmení a národnost
- **Příběh:**
	- Rumunská mafie byla úspěšně zlikvidována, můžete znovu pouštět obyvatele 4. a 5. patra. Nově máme podezření, že obyvatelé bloku 11 pomalu, ale jistě přebírají vedení našeho alkoholického kroužku. Kvůli zachování pevné moci nikoho z 11. bloku nepouštějte."


#### UDÁLOSTI

1.
Blok x během noci schvátily plameny. Jelikož jsme ze zákona nuceni poskytnou poškozeným azyl, je rezidentům bloku x dočasně povolený vstup do budovy.

## POSTAVY

#### SUZ inspektor
- První den nám dá uvítání do práce

#### Skibidi kid

- Poprvé se ukáže druhý den, přijde se zaregistrovat na kolej, bude mít něco špatně
	- Správně by se měl poslat pryč, když se pustí dovnitř stane se hovnovým démonem
- Ukáže se znova až čtvrtý den, opět s registrací, tentokrát by měl mít všechno dobře
	- když ho pustím, nastane skibidi toilet
- Potom může chodit random jako klasické npc, bude mít vlastní styl dialogu

#### VŠE student

- Poprvé se objevi až třetí den, přijde s falešnou kolejenkou 
	- Měl bych ho poslat pryč
- Znovu se ukáže az pátý den, tentokrát má všechno dobře
	- Pokud ho nepustím, dá mi bombu do okénka, kterou musím zneškodnit
- Potom se může taky objevovat náhodně s dalšíma NPC, bude mít unikátní dialogy

#### Metro typek

- Ukazuje se náhodně, vlastní styl dialogů






