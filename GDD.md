# KOLEJENKU, pls

### Zasazení
Ve hře se ujmete role "kolejbáby" na jednom z bloků Strahovských kolejí. Cílem hry je získat dostatek peněz na zasloužený důchod a zároveň udržení stavu kolejí na přijatelné úrovni.

Hlavní výplní je kontrolování kolejenek studentů (ala Papers, please) doufajících se dostat uvnitř bloku. Po skončení šichty u vrátnice se ještě bude muset vyřešit nějaký problém na koleji ve formě klikací mini-hry. Nezvládnutí mini-hry znamená strhnutí peněz z platu a zhoršení stavu kolejí.

### Inspirace
* Papers, please
* klikací hry typu najdi objekt
* Whack-a-mole

## Hlavní část
Kontrola dokumentů studentů.

* Rozhodnutí ovlivňují ostatní části hry

### Typy dokumentů
* může být 1 až n vadných dokumentů
#### Vstup
* Kolejenka
#### Registrace
* Občanka (pro Čechy\Slováky)
* Pas (pro cizince)
* Potvrzení o studiu

### Přijmutí / Odmítnutí
Tlačítka Otevřít/Neotevřít dveře.

### Technické bláboly
* Postavy jsou už před generací označeny jestli mají vše v pořádku, či nikoliv
* Postavy jsou prefaby, které se budou klonovat a uprovavovat
    * Všechno ok
    * něco špatně
    * příběhové

### Inspekce
Přichází v pravidelném intervalu, např. 5 dní.
Pokud hráč dostane 3 striky, hra končí.

## Mini-hry
Mini-hry vrací jen výsledek Game Controlleru.
### Kuchyň
* švábi
* utírání bordelu

### Chodba
* pohled z první osoby
* posouvání šipkami na hranách obrazovky
#### Varianta I
Jeden nebo více věšáků, hráč musí najít vlastníky.
#### Varianta II
Věšáky vedle dveří, hráč je bude přesouvat do dveří. Dveře se otevřou a v nich může stát postava, která hodí úsečný komentář.

### Pokoj
#### Varianta I
Hledání kontrabantu
* interakce s nábytkem
* může se zabavit i ne-kontrabant -> stížnost
#### Varianta II
* ničení štěnic

### Záchody
* punishment za otevření dveří, kde je čistej záchod
* umývání houbičkou