# KOLEJENKU, pls
Hra je silně inspirovaná hrou Papers Please. Je zasazena do prostředí Strahovských kolejí, kde se hráč v roli kolejbáby snaží udržet bezpečí a pořádek svého bloku. Vizuál hry kombinuje doomerskou slav atmosféru s komunistickými prvky, přičemž jednotlivé scény jsou zpracovány v pixel artu. Pro tvorbu hry bylo použito Unity.

### Příběh
Hráč po úmorném výběrovém řízení získává práci snů kolejbáby na Strahovských kolejích. Jeho cílem je našetřit si na vysněnou dovolenou na Kanárských ostrovech. Musí tak nejen kontrolovat studenty kolejí, ale aktivně se snažit zneškodlit štěnicový mor, hovnového démona nebo potlačit teroristické skupiny. Stihne si hráč našetřit na vysněnou dovolenou nebo se po šílených údálostech na Strahovských kolejích jeho osud vydá jiným směrem?

### Dokumentace

[Statický svět](https://gitlab.fit.cvut.cz/BI-VHS/b241_projects/nefitnuto/-/blob/main/documentation/Staticky_svet.md)

[Dynamický svět](https://gitlab.fit.cvut.cz/BI-VHS/b241_projects/nefitnuto/-/blob/main/documentation/Dynamicky_svet.md)

[Komplexní svět](https://gitlab.fit.cvut.cz/BI-VHS/b241_projects/nefitnuto/-/blob/main/documentation/Komplexni_svet.md)

#### Zdroje
[Zdroje](https://gitlab.fit.cvut.cz/BI-VHS/b241_projects/nefitnuto/-/blob/main/documentation/Zdroje.md)